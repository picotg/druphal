import {
    createDropdown,
    Model,
    addListToDropdown,
} from 'ckeditor5/src/ui';
import HaltagView from './haltagview';
import { Collection } from 'ckeditor5/src/utils';

/**
 * baloon editor for changing min and max dates.
 */
export default class HaltagDatesView extends HaltagView {
    _buildForm() {
        const { t } = this.locale;
        const minDateField = createDropdown(this.locale);
        minDateField.panelView.template.attributes.class.push('druphal-dropdown');

        minDateField.buttonView.set({
            label: (('min_year' in this._currentData) && (this._currentData.min_year != 0))?this._currentData.min_year:'de',
            withText: true
        })

        const items = new Collection();

        
        items.add({
            type: 'button',
            model: new Model({
                id: 0,
                withText: true,
                label: t('no date'),
            })
        })
        for (var i = new Date().getFullYear(); i > 1970 ; i--) {
            items.add({
                type: 'button',
                model: new Model({
                    id: i,
                    withText: true,
                    label: i,
                })
            })
        }

        addListToDropdown(minDateField, items);
        this.listenTo(minDateField, 'execute', eventInfo => {
            var newValue = eventInfo.source.id;
            minDateField.buttonView.set({label: (newValue!=0)?newValue:t('from')});
            if(newValue != 0) {
                this._currentData.min_year = newValue;
            } else {
                delete this._currentData.min_year;
            }
        });

        
        const maxDateField = createDropdown(this.locale);
        maxDateField.panelView.template.attributes.class.push('druphal-dropdown');

        maxDateField.buttonView.set({
            label: (('max_year' in this._currentData) && (this._currentData.max_year != 0))?this._currentData.max_year:t('to'),
            withText: true
        })

        addListToDropdown(maxDateField, items);
        this.listenTo(maxDateField, 'execute', eventInfo => {
            var newValue = eventInfo.source.id;
            maxDateField.buttonView.set({label: (newValue!=0)?newValue:t('to')});
            this._currentData.max_year = newValue;
            if(newValue != 0) {
                this._currentData.max_year = newValue;
            } else {
                delete this._currentData.max_year;
            }
        });

        this._buildedForm['min_date'] = minDateField;
        this._buildedForm['max_date'] = maxDateField;
        return Object.values(this._buildedForm);
    }
}
