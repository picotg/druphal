import {
    createDropdown,
    Model,
    addListToDropdown,
    SwitchButtonView,
} from 'ckeditor5/src/ui';
import HaltagView from './haltagview';
import { Collection } from 'ckeditor5/src/utils';


/**
 * Contextual balloon editor for render option.
 */
export default class HaltagRenderView extends HaltagView {
    _buildForm() {
        const { t } = this.locale;
        const templateList = this._druphalConfig.template_list;
        const addedKey = [
            'template_local',
            'template_filename',
            'template_engine',
            'template',
            'result_fields',
        ];
        const renderField = createDropdown(this.locale);
        renderField.panelView.template.attributes.class.push('druphal-dropdown');

        renderField.buttonView.set({
            label: ('selected_template' in this._currentData)?this._currentData.selected_template:t('Default Hal style'),
            withText: true
        })

        const items = new Collection();

        items.add({
            type: 'button',
            model: new Model({
                id: 'none',
                withText: true,
                label: t('Default Hal style'),
            })
        })

        for(const key in templateList) {
            items.add({
                type: 'button',
                model: new Model({
                    withText: true,
                    label: key,
                })
            })
        }

        addListToDropdown(renderField, items);

        this.listenTo(renderField, 'execute', eventInfo => {
            for(const key of addedKey) {
                if(key in this._currentData) delete this._currentData[key];
            }
            if(eventInfo.source.id != 'none') {
                var newValue = eventInfo.source.label;
                renderField.buttonView.set({label: newValue});
                this._currentData = {...this._currentData, ...templateList[newValue]};
                if('fields' in this._currentData) {
                    this._currentData.result_fields = this._currentData.fields;
                    delete this._currentData.fields;
                }
                if('template' in this._currentData) this._currentData.template = this._currentData.template.replace(/<br ?\/?>/g, "%%NEWLINE%%");
                this._currentData.selected_template = newValue;
            } else {
                delete this._currentData.selected_template;
                renderField.buttonView.set({label: t('Default Hal style')});
            }
        });

        this._buildedForm['selected_template'] = renderField;

        const renderByDoctypeField = new SwitchButtonView();

        renderByDoctypeField.set( {
            label: t('by doctypes'),
            withText: true,
            isOn: ('renderByDoctype' in this._currentData)?true:false,
        } );

        this.listenTo(renderByDoctypeField, 'execute', () => {
            renderByDoctypeField.isOn = !renderByDoctypeField.isOn;
            if(renderByDoctypeField.isOn) {
                this._currentData.renderByDoctype = true;
            } else {
                delete this._currentData.renderByDoctype;
            }
        } );

        this._buildedForm['renderByDoctype'] = renderByDoctypeField;

        return Object.values(this._buildedForm);
    }
}
