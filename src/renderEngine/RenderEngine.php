<?php
/**
 * 
 * Abstract render engine definition.
 * 
 * @author Gaël PICOT
 * 
 * drupHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 * 
 */
namespace Drupal\druphal\renderEngine;
require_once \Drupal::service('extension.list.module')->getPath('druphal').'/vendor/autoload.php';

use Drupal\druphal\parameters\QueryParameters;
use uga\hallib\queryDefinition\DataContainer;
use uga\hallib\QueryIterator;

/**
 * Abstract render engine definition..
 */
abstract class RenderEngine extends DataContainer {

    /**
     * Hal query parameters.
     */
    protected QueryParameters $queryParameters;

    /**
     * Render engines listes.
     */
    protected static array $engineListe = [
        'CSL' => CSLEngine::class,
        'URLOnly' => URLOnlyEngine::class,
        'twig' => UseTwigEngine::class,
    ];

    /**
     * Add new render engine.
     * 
     * @param string $name render engine name
     * @param string $engineClass render engine class
     */
    public static function addEngine(string $name, string $engineClass) {
        static::$engineListe[$name] = $engineClass;
    }

    /**
     * Create render engine using $values->template_engine value to choose type.
     * 
     * @param stdClass $values parameter with template_engine
     */
    public static function createRenderEngine(\stdClass $values=null): RenderEngine {
        if(($values !== null) && isset($values->template_engine) && isset(static::$engineListe[$values->template_engine])) {
            return new static::$engineListe[$values->template_engine]($values);
        } else {
            return new UseTwigEngine($values);
        }
    }

    /**
     * Constructeur
     * create QueryParameters instance
     */
    public function __construct(\stdClass $values=null) {
        parent::__construct($values);
        $this->queryParameters = new QueryParameters($values);
    }

    /**
     * Render for array of Hal documents.
     * 
     * @param array $data array of Hal documents.
     * @return string result of render
     */
    abstract public function render_datas(array $data = null): string;

    /**
     * Render from QueryParameters.
     */
    public function render(): string {
        if ($this->queryParameters->renderByDoctype) {
            return $this->renderByDoctype();
        } else {
            return $this->render_datas();
        }
    }

    /**
     * Render by doctypes.
     */
    public function renderByDoctype(): string {
        if(!in_array('docType_s',explode(',', $this->queryParameters->result_fields))) {
            $this->queryParameters->result_fields .= ',docType_s';
        }
        $queryIterator = new QueryIterator($this->queryParameters->makeQuery());
        $librairiesDir = \Drupal::service('extension.list.module')->getPath('druphal').'/libraries/';
        $docByDoctype = [];
        foreach($queryIterator as $doc) {
            if(!isset($docByDoctype[$doc->docType_s])) {
                $docByDoctype[$doc->docType_s] = [$doc];
            } else {
                array_push($docByDoctype[$doc->docType_s], $doc);
            }
        }
        $doctypeFile = $librairiesDir.'data/doctype.json';
        $doctypeData = json_decode(file_get_contents($doctypeFile));
        $doctypes = [];
        foreach($doctypeData->response->result->doc as $doc) {
            $doctypes[$doc->str[0]] = $doc->str[1];
            if(isset($doc->arr)) {
                foreach($doc->arr->doc as $docL2) {
                    $doctypes[$docL2->str[0]] = $docL2->str[1];
                }
            }
        }
        $renderedByDoctype = [];
        foreach(array_keys($doctypes) as $key) {
            if(isset($docByDoctype[$key])){
                $renderedByDoctype[$doctypes[$key]] = $this->render_datas($docByDoctype[$key]);
            }
        }
        $twig = \Drupal::service('twig');
        return $twig
            ->createTemplate(file_get_contents($librairiesDir.'templates/doctype_separation.twig'), NULL)
            ->render(['renderedByDoctype' => $renderedByDoctype]);
    }
}
