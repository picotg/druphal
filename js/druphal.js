class ParameterValue {
    min_year = '0'
    max_year = '0'
    rows = '100'
    result_fields = 'citationFull_s'
    template = ''
    authorQueryType = 'classical'
    author = ''
    structure = ''
    query = ''
    sort = 'producedDate_s'
    sort_type = 'desc'
    doctypes = ''
    currentText = null
    range = null
    template_local = 'fr-FR'
    template_filename = ''
    template_engine = 'twig'
    instance = ''
    renderByDoctype = false
    structureQueryType = 'acronym'

    get editor_template() {
        return this.template.replace(/%%NEWEDITELINE%%/gm, '\n').replace(/%%NEWLINE%%/g, "<br>")
    }

    get page() {
        if(this.structure !== '') {
            return 'druphalStructure';
        } else if(this.query !== '') {
            return 'druphalCustom';
        }
        return 'druphalAuthor';
    }

    getCurrentValue(currentRange) {
        var halRegex = /\[hal\](.*?)\[\/hal]/;
        var match = currentRange.startContainer.getText().match(halRegex);
        if(match !== null) {
            var currentValues = JSON.parse(match[1]);
            for(const valueName in currentValues) {
                this[valueName] = currentValues[valueName];
            }
            this.currentText = match[0]
            this.range = currentRange
            return true;
        } else {
            return false;
        }
    }
}
