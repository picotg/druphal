import {
    SwitchButtonView,
    ButtonView,
} from 'ckeditor5/src/ui';
import HaltagView from './haltagview';
import HaltaginstancesView from './haltaginstancesview';


/**
 * Contextual balloon editor for instance option.
 */
export default class HaltaginstanceView extends HaltagView {
    _buildForm() {
        const { t } = this.locale;
        const instanceSwitch = new SwitchButtonView(this.locale);
        const default_instance = JSON.parse(this._druphalConfig.default_instance)
        
        instanceSwitch.set( {
            label: default_instance[0],
            withText: true,
            isOn: ('instance' in this._currentData) && (this._currentData.instance === default_instance[1])
        } );

        this.listenTo(instanceSwitch, 'execute', eventInfo => {
            var source = eventInfo.source;
            source.isOn = !source.isOn;
            if(source.isOn) {
                this._currentData.instance = default_instance[1];
            } else {
                delete this._currentData.instance;
            }
        });

        const otherButtonView = new ButtonView();

        otherButtonView.set({
            label: t('other instance'),
            withText: true,
        })

        otherButtonView.delegate('execute').to(this, 'showOtherBalloon');

        this._buildedForm['instance'] = instanceSwitch
        this._buildedForm['other'] = otherButtonView
        return Object.values(this._buildedForm);
    }

    get otherBalloon () {
        return HaltaginstancesView;
    }
}
