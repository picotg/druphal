/**
 * @file HalTag editor master plugin. this file's purpose is to integrate all the
 * separate parts of the plugin before it's made discoverable via index.js.
 */

import HaltagEditorEditing from './haltageditorediting';
import HaltagEditorUI from './haltageditorui';
import { Plugin } from 'ckeditor5/src/core';

/**
 * HalTag editor master plugin.
 */
export default class HaltagEditor extends Plugin {
  static get requires() {
    return [HaltagEditorEditing, HaltagEditorUI];
  }
}
