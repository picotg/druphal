import {
    createDropdown,
    Model,
    addListToDropdown,
} from 'ckeditor5/src/ui';
import HaltagView from './haltagview';
import { Collection } from 'ckeditor5/src/utils';


/**
 * Contextual balloon editor for sorting option.
 */
export default class HaltagSortView extends HaltagView {
    _buildForm() {
        const { t } = this.locale;
        const sortField = createDropdown(this.locale);
        sortField.panelView.template.attributes.class.push('druphal-dropdown');

        sortField.buttonView.set({
            label: ('sort' in this._currentData)?t('title'):t('date'),
            withText: true
        })

        const sortItems = new Collection();

        
        sortItems.add({
            type: 'button',
            model: new Model({
                id: 'date',
                withText: true,
                label: t('date'),
            })
        })
        
        sortItems.add({
            type: 'button',
            model: new Model({
                id: 'titre',
                withText: true,
                label: t('title'),
            })
        })

        addListToDropdown(sortField, sortItems);

        this.listenTo(sortField, 'execute', eventInfo => {
            var newValue = eventInfo.source.id;
            sortField.buttonView.set({label: newValue});
            if(newValue != 'date') {
                this._currentData.sort = 'title_sort';
            } else {
                delete this._currentData.sort;
            }
        });

        
        const sortTypeField = createDropdown(this.locale);
        sortTypeField.panelView.template.attributes.class.push('druphal-dropdown');

        sortTypeField.buttonView.set({
            label: ('sort_type' in this._currentData)?t('ascending'):t('descending'),
            withText: true
        })

        const sortTypeItem = new Collection();

        
        sortTypeItem.add({
            type: 'button',
            model: new Model({
                id: 'asc',
                withText: true,
                label: t('ascending'),
            })
        })
        
        sortTypeItem.add({
            type: 'button',
            model: new Model({
                id: 'desc',
                withText: true,
                label: t('descending'),
            })
        })

        addListToDropdown(sortTypeField, sortTypeItem);

        this.listenTo(sortTypeField, 'execute', eventInfo => {
            var newValue = eventInfo.source.id;
            sortTypeField.buttonView.set({label: (newValue != 'desc')?t('ascending'):t('descending')});
            if(newValue != 'desc') {
                this._currentData.sort_type = 'asc';
            } else {
                delete this._currentData.sort_type;
            }
        });

        this._buildedForm['sort'] = sortField;
        this._buildedForm['sort_type'] = sortTypeField;
        return Object.values(this._buildedForm);
    }
}
