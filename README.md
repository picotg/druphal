# drupHal

Recherche et mise en forme des dernières publications Hal d'une structure ou d'un chercheur.

## Table des matières

 * Les différentes parties
 * Installtion DrupHal
 * Tests phpunit
 * Liste des fichiers et leurs utilité

## Les différentes parties

### filtre Hal

Le filtre Hal (src/Plugin/Filter/FilterHal.php) est un [filtre personalisé](https://www.drupal.org/forum/support/module-development-and-code-questions/2020-01-28/creating-a-custom-text-filter) texte qui à pour mission d'interprèter les balises [hal]...[/hal] dans les textes et les remplace par le résultat de la requête mis en form par le moteur de rendu.

### Moteurs de rendu

Les moteur de Rendu (src/renderEngine) servent à executer la requête et à transformer le résultat en chaîne de caractères.

- UseTwigEngine (src/renderEngine/UseTwigEngine.php) : qui retourne le résultat d'un rendu [Twig](https://twig.symfony.com/)
- CSLJsonFromHalDoc (src/renderEngine/CSLJsonFromHalDoc.php) : utilise [citeproc-php](https://github.com/seboettg/citeproc-php) pour rendre une feuille de style CSL
- URLOnlyEngine (src/renderEngine/CSLJsonFromHalDoc.php) : Renvoie juste l'url de la requête

### plugin CKEditor 4

Le plugin CkEditor 4 permmet d'ajouter à CkEditor 4 un icon pour ajouter et modifier des balise [hal]...[/hal] bien former.

Le pluging javascript pour l'ajout et l'édition des balises [hal]...[/hal] pour CkEditor 4 est librairies/plugins/druphal/plugin.js. Le formulaire pour l'intégration dans Drupal est disponible ici : src/Plugin/CKEditorPlugin/DrupHalPlugin.php

### plugin CkEditor 5

Le plugin remplace les balise [hal]...[/hal] par un editeur direct.

Pour la création de plugin CkEditor 5, on doit utilisé [Webpack](https://webpack.js.org/). Les fichier qui le compose sont disponible dans le répertoire jas/ckeditor5_plugins/druphal/. Ou l'on peut trouver les fichierr de traduction (/lang) et les fichier source (/src). Le formulaire pour les option est dispnilbe dans src/Plugin/CkEditor5Plugin/DrupHalPluginCk5.php.

#### construction du fichier

CkEditor 5 étant conçut pour utilisé [Webpack](https://webpack.js.org/), il faut construire le plugin en utilisant la commande yarn build dans le repertoir js du module.

## Installtion DrupHal

### Installation Drupal

Les détails de l'installation peuvent être trouver [ici](https://www.drupal.org/project/drupal/).

L'installation :
```
composer create-project drupal/recommended-project "install-directory"
```

### Installation Druphal

1) Copier les fichier dans web/module/druphal
2) aller dans Extension et installer
3) executer composer update dans le répertoire du module DrupHal

## Tests phpunit

Pour configurer les test unitaire dans drupal ([version détaillée](https://www.drupal.org/docs/automated-testing/phpunit-in-drupal/running-phpunit-tests)), il faut :
1) installer toute les dépendances pour le dévelopement

```
composer require drupal/core-dev --dev --update-with-all-dependencies
```
2) créé un fichier phpunit.xml avec les option nécessaire dans le répertoir web de drupal (par exemple en copiant core/phpunit.xml.dist)
3) Ajouter les test de DurpHal dans le fichier:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<phpunit [...]>
[...]
  <testsuites>
    [...]
    <testsuite name="druphal">
      <file>.\modules\druphal\tests\src\Unit\HallibUnitCase.php</file>
    </testsuite>
  </testsuites>
</phpunit>
```
4) lancer phpunit dans le répértoire web
 
```
..\vendor\bin\phpunit
```

## Liste des fichiers et leurs utilité.

- druphal.module : fichier definissant les différent [hooks drupal](https://api.drupal.org/api/drupal/core%21core.api.php/group/hooks/10)
- druphal.info.yml : définition du plugin
- druphal.install : fichier executer à l'installation du plugin (initialisation des données)
- druphal.ckeditor5.yml : configuration plugn CkEditor 5
- druphal.libraries.yml : definition des différente bibliothèque js et css

### config

fichier de config

- config/ultimate_cron.job.druphal_update_default.yml : fichier de configurationpour 

#### schema

- schema pour le stockage de dommées

### css

feuille de style

- css/csl.css : feulle de style pour la correction de laffichage de certains rendu CSL
- css/druphal-ckeditor.css : feulle de style pour ckeditor 4
- css/druphal-ckeditor5.css : feulle de style pour ckeditor 5

### doc

Documentation utilisateurs

#### img

Image pour la documentation utilisateurs

- doc/doc.fr.md : doc en français permétant de généré la doc html en utilisant [pandoc](https://pandoc.org/) avec la command :

```
pandoc -o doc.fr.html doc.fr.md
```
- doc/doc.fr.html : doc en français en html.
- doc/doc.md : doc en anglait permétant de généré la doc html en utilisant [pandoc](https://pandoc.org/).
- doc/doc.html : doc en anglait en html.

### js

sources javascript divers

- js/csl.js : implémentation manuelle de la pseudo-classe CSS : has toujours pas supporté par firefox par défaut
- js/druphal.js : class décricct les paramétre côté JS.
- js/template_list_edit.js : javscript utilisé pour le formulaire d'édition des templates

#### build

version construite du plugin CkEditr

##### js/ckeditor5_plugins/druphal

source javascripte du plgin CkEditor5

###### src

- js/ckeditor5_plugins/druphal/srv/haltagcommand.js : command pour l'ajout d'une balse Hal par défaut
- js/ckeditor5_plugins/druphal/srv/haltagdatesview.js : vue pour l'ajout de menu de selection de dates
- js/ckeditor5_plugins/druphal/srv/haltagdoctypesview.js  : vue pour les des types documents chercher
- js/ckeditor5_plugins/druphal/srv/haltageditor.js : fichier javascript principale
- js/ckeditor5_plugins/druphal/srv/haltageditorediting.js : parti editeur du plugin qui constriot de widget remplaçant les balis Hsl.
- js/ckeditor5_plugins/druphal/srv/haltageditorui.js : gestions générale des [bulle contextuel](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/balloon-editor.html)
- js/ckeditor5_plugins/druphal/srv/haltaginstancesview.js : vus pour le choix du portail quand il n'y a pas de portail par défaut
- js/ckeditor5_plugins/druphal/srv/haltaginstanceview.js : vus pour le choix du portail quand il y a  portail par défaut
- js/ckeditor5_plugins/druphal/srv/haltagquerytypeview.js : type de recherche dans Hal
- js/ckeditor5_plugins/druphal/srv/haltagrenderview.js : option de rendue
- js/ckeditor5_plugins/druphal/srv/haltagsortview.js : option de tris
- js/ckeditor5_plugins/druphal/srv/haltagtemplateengineview.js : choix du template
- js/ckeditor5_plugins/druphal/srv/haltagview.js : gestions générale des [bulle contextuel](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/balloon-editor.html)
- js/ckeditor5_plugins/druphal/srv/index.js : just le chagement du plugin
- js/ckeditor5_plugins/druphal/srv/showballooncommand.js : command pour l'affichage des bull de context

### librairies

#### data

- librairies/data/default_base_template.json : liste des template par défaut
- librairies/data/doctype.json : liste des type de document possible
- librairies/data/instance.json : liste des portail

#### plugins/druphal

- librairies/plugins/druphal/plugins : plugin CkEditor 4

#### templates

- maj_status.twig : affichage du status de mise à jour
- doctype_separation.twig : affichage par type de documents

### src

source PHP du module

#### DataManager

- UpdateHalDataListe.php : gestion de la mise à jour de la liste des instances et de la liste des types de documents

#### parameters

- src/parameters/QueryParameters.php : gestion des parametre d'une balise [hal]...[/hal] et construction de la requête à partir de ces paramêtres

#### plugin

##### CKEditor5Plugin

- src/plugin/CKEditor5Plugin/DrupHalPluginCk5.php : intégration du plugin CkEditor 5 dans drupal avec formulaire de configuration

##### CKEditorPlugin

- src/plugin/CKEditorPlugin/DrupHalPlugin.php  intégration du plugin CkEditor 4 dans drupal avec formulaire de configuration

##### Filter

- src/plugin/Filter/FilterHal.php : filtre

#### renderEngine

Moteur de rendu pour une requête

- renderEngine/CSLEngine.php : utilisation de feuilles de style CSL
- renderEngine/RenderEngine.php : classe abstraite pour la création d'un moteur de rendu
- renderEngine/URLOnlyEngin.php : renvoi uniquement l'URL construite à partir des paramêtres de la requête
- renderEngine/UseTwigEngine.php : Moteur de rendu utilisant Twig
