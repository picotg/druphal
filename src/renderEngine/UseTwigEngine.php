<?php
/**
 * 
 * Render engine using Twig.
 * 
 * @author Gaël PICOT
 * 
 * drupHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 * 
 */
namespace Drupal\druphal\renderEngine;

use uga\hallib\QueryIterator;

/**
 * Render engine using Twig.
 */
class UseTwigEngine extends RenderEngine {
    /**
     * twig template.
     */
    protected string $template = '{% for item in items%}{{ item.citationFull_s|raw }}<br /><br />{% endfor %}';

    /**
     * {@inheritdoc}
     */
    public function render_datas(array $data = null):string {
        if($data === null) {
            $data = new QueryIterator($this->queryParameters->makeQuery());
        }
        $twig = \Drupal::service('twig');
        $template = str_replace('%%NEWLINE%%', '<br>', $this->template);
        $template = str_replace('%%NEWEDITELINE%%', '', $template);
        return $twig
            ->createTemplate($template, NULL)
            ->render(['items' => $data]);
    }
}
