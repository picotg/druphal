/**
 * CKEditor 5 plugins do not work directly with the DOM. They are defined as
 * plugin-specific data models that are then converted to markup that
 * is inserted in the DOM.
 * 
 * This file has the logic for defining the HalTag editor model, and for how it
 * is converted to standard DOM markup.
 */

import { Plugin } from 'ckeditor5/src/core';
import { toWidget } from 'ckeditor5/src/widget';
import { Widget } from 'ckeditor5/src/widget';
import insertHalTagCommand from './haltagcommand';
import showBalloonCommand from './showballooncommand';

export default class HaltagEditorEditing extends Plugin {

  static get requires() {
    return [Widget];
  }

  init() {
    this._defineSchema();
    this._defineConverters();
    this.editor.commands.add(
      'insertHalTag',
      new insertHalTagCommand(this.editor),
    );
    var showBalloon = new showBalloonCommand(this.editor);
    showBalloon.init()
    this.editor.commands.add(
      'showBalloon',
      showBalloon,
    );
  }

  /*
   * This registers the structure that will be seen by CKEditor 5 as
   * <halTag>
   * </halTag>
   * with all is attributes
   */
  _defineSchema() {
    // Schemas are registered via the central `editor` object.
    const schema = this.editor.model.schema;

    schema.register('halTag', {
      allowWhere: '$text',
      allowAttributes: [
        'author',
        'authorQueryType',
        'rows',
        'selected_template',
        'template_engine',
        'template_filename',
        'template_local',
        'min_year',
        'max_year',
        'result_fields',
        'template',
        'structure',
        'query',
        'sort',
        'sort_type',
        'doctypes',
        'instance',
        'renderByDoctype',
        'structureQueryType',
      ],
      isObject: true,
      isBlock: true,
    });
  }

  /**
   * Converters determine how CKEditor 5 models are converted into markup and
   * vice-versa.
   */
  _defineConverters() {
    // Converters are registered via the central editor object.
    const { conversion, config } = this.editor;
    const druphalConfig = config.get('druphal');
    const { t } = this.editor.locale;

    // Upcast Converters: determine how existing HTML is interpreted by the
    // editor. These trigger when an editor instance loads.

    conversion.for('upcast').add(dispatcher => {
      dispatcher.on('text', (evt, data, { schema, consumable, writer }) => {
        let position = data.modelCursor;

        if (!consumable.test(data.viewItem)) return;

        consumable.consume(data.viewItem);

        let modelCursor = position;

        var node
        for (const part of data.viewItem.data.split(/(\[hal\].*?\[\/hal])/)) {
          if(part.startsWith('[hal]')) {
            try {
              var jsonData = JSON.parse(part.slice(5, -6));
              node = writer.createElement('halTag', jsonData);
            } catch(e) {
              node = writer.createText(part);
            }
          } else {
            node = writer.createText(part);
          }
          writer.insert(node, modelCursor);
          modelCursor = modelCursor.getShiftedBy( node.offsetSize );
        }

        data.modelRange = writer.createRange(position, modelCursor);
        data.modelCursor = data.modelRange.end;
      });
    });

    // create widget éditor from halTag element.
    conversion.for('editingDowncast').elementToStructure( {
      model: 'halTag',
      view: ( element, { writer } ) => {
        var datas = {};
        var mode = 'author';
        var mainLabel = t('Name');
        for(const key of element.getAttributeKeys()) {
          datas[key] = element.getAttribute(key);
        }
        var titre = '';
        if('author' in datas) {
          titre = t('[druphal] Hal document display for an author');
          mode = 'author';
          if('authorQueryType' in datas && datas.authorQueryType === 'idHal') {
            mainLabel = t('idHal');
          }
        } else if('structure' in datas) {
          titre = t('[druphal] Hal document display for a structure');
          mode = 'structure';
          mainLabel = t('Full name');
          if('structureQueryType' in datas && datas.structureQueryType === 'acronym') {
            mainLabel = t('Acronym');
          }
        } else {
          titre = t('[druphal] Hal document display a custom query');
          mode = 'query';
          mainLabel = t('Query');
        }
        const queryEditor = writer.createRawElement('span', {
          class: 'halTagQuery',
          contenteditable: true,
        }, domElement => {
          domElement.innerText = datas[mode];

          // selection de l'élément model
          domElement.addEventListener('mousedown', () => {
            const model = this.editor.model;
            const selectedElement = model.document.selection.getSelectedElement();

            if (selectedElement !== element) {
              model.change(writer => writer.setSelection(element, 'on'));
            }
          }, true);

          domElement.addEventListener('DOMSubtreeModified', () => {
            const model = this.editor.model;
            const selectedElement = model.document.selection.getSelectedElement();
            model.change(writer => {
              writer.setAttribute(mode, domElement.innerText, selectedElement);
            })
          }, true);
        });
        writer.setAttribute('data-cke-ignore-events', 'true', queryEditor);
        var halData = []
        if(mode !== 'query') {
          halData = [
            writer.createContainerElement('div', {class: 'halTagTitle'}, [writer.createText(titre)]),
            writer.createContainerElement('div', {}, [
              writer.createContainerElement('span', {class: 'halTagQueryType'}, [writer.createText(mainLabel)]),
              writer.createText(' : '),
              queryEditor,
            ]),
          ];
        } else {
          halData = [
            writer.createContainerElement('div', {class: 'halTagTitle'}, [writer.createText(titre)]),
            writer.createContainerElement('div', {}, [
              writer.createText(mainLabel + ' : '),
              queryEditor,
            ]),
          ];
        }
        if(mode !== 'query') {
          var doctypesElement = null;
          if('doctypes' in datas) {
            var doctypesMessage = '';
            var doctypes = datas['doctypes'].substring(1, datas['doctypes'].length-1).split('+OR+');
            for(const doctype of doctypes) {
              doctypesMessage += doctype + ', '
            }
            doctypesElement = writer.createText(doctypesMessage.substring(0, doctypesMessage.length-2));
          } else {
            doctypesElement = writer.createText(t('all', 'doctypes'));
          }
          halData.push(writer.createContainerElement('div', {}, [
            writer.createText(t('Doctypes') + ': '),
            writer.createContainerElement('span', {class: 'halTagDoctype'}, [doctypesElement])
          ]));
          halData.push(writer.createContainerElement('div', {}, [
            writer.createText(t('Instance: ')),
            writer.createContainerElement('span', {class: (druphalConfig.default_instance !== '')?'halTagInstance':'halTagInstances'}, [writer.createText(('instance' in datas)?datas.instance:'aucun')])
          ]));
          var datesMessage = '';
          if('min_year' in datas || 'max_year' in datas) {
            datesMessage += '[ ';
            datesMessage += ('min_year' in datas)?datas['min_year']:'*';
            datesMessage += ' à ';
            datesMessage += ('max_year' in datas)?datas['max_year']:'*';
            datesMessage += ' ]';
          } else {
            datesMessage += t('all', 'dates');
          }
          halData.push(writer.createContainerElement('div', {}, [
            writer.createText(t('Dates: ')),
            writer.createContainerElement('span', {class: 'halTagDates'}, [writer.createText(datesMessage)])
          ]));
          var rows = 100;
          if('rows' in datas) {
            rows = datas.rows;
          }
          const rowsEditor = writer.createRawElement('span', {
            type: 'text',
            class: 'halTagRows',
            contenteditable: true,
          }, domElement => {
            domElement.innerText = rows;
  
            // selection de l'élément model
            domElement.addEventListener('mousedown', () => {
              const model = this.editor.model;
              const selectedElement = model.document.selection.getSelectedElement();
  
              if (selectedElement !== element) {
                model.change(writer => writer.setSelection(element, 'on'));
              }
            }, true);

            domElement.addEventListener('DOMSubtreeModified', () => {
              const model = this.editor.model;
              const selectedElement = model.document.selection.getSelectedElement();
              model.change(writer => {
                writer.setAttribute('rows', domElement.innerText, selectedElement);
              })
            }, true);
          });
          writer.setAttribute('data-cke-ignore-events', 'true', rowsEditor);
          halData.push(writer.createContainerElement('div', {}, [
            writer.createText('nombres de resultats: '),
            rowsEditor,
          ]));
          var sortMessage = t('Sort ');
          if('sort' in datas || 'sort_type' in datas) {
            sortMessage += t('on field ');
            sortMessage += ('sort' in datas)?'titre':'date';
            sortMessage += t(' order by ');
            sortMessage += ('sort_type' in datas)?t('ascending'):t('descending');
          } else {
            sortMessage += t('default');
          }
          halData.push(writer.createContainerElement('div', {}, [
            writer.createContainerElement('span', {class: 'halTagSort'}, [writer.createText(sortMessage)])
          ]));
          var renderMessage = '';
          if('selected_template' in datas || 'renderByDoctype' in datas) {
            renderMessage += ('selected_template' in datas)?datas.selected_template:t('Default Hal style');
            renderMessage += ('renderByDoctype' in datas)?(' ' + t('by doctypes')):'';
          } else {
            renderMessage += t('default');
          }
          halData.push(writer.createContainerElement('div', {}, [
            writer.createText(t('Render option: ')),
            writer.createContainerElement('span', {class: 'halTagRender'}, [writer.createText(renderMessage)])
          ]));
        } else {
          halData.push(writer.createContainerElement('div', {}, [
            writer.createText(t('Render engine: ')),
            writer.createContainerElement('span', {class: 'halTagTemplateEngine'}, [writer.createText(('template_engine' in datas)?datas.template_engine:'twig')])
          ]));
          if(!('template_engine' in datas) || datas.template_engine == 'twig') {
            const resultFieldsEditor = writer.createRawElement('span', {
              class: 'halTagResultFields',
              contenteditable: true,
            }, domElement => {
              domElement.innerText = ('result_fields' in datas)?datas.result_fields:'vide';
    
              // selection de l'élément model
              domElement.addEventListener('mousedown', () => {
                const model = this.editor.model;
                const selectedElement = model.document.selection.getSelectedElement();
    
                if (selectedElement !== element) {
                  model.change(writer => writer.setSelection(element, 'on'));
                }
              }, true);
  
              domElement.addEventListener('DOMSubtreeModified', () => {
                const model = this.editor.model;
                const selectedElement = model.document.selection.getSelectedElement();
                model.change(writer => {
                  writer.setAttribute('result_fields', domElement.innerText, selectedElement);
                })
              }, true);
            });
            writer.setAttribute('data-cke-ignore-events', 'true', resultFieldsEditor);
            halData.push(writer.createContainerElement('div', {}, [
              writer.createText('Result fields: '),
              resultFieldsEditor,
            ]));
            const templateEditor = writer.createRawElement('textarea', {class: 'halTagTemplate'}, domElement => {
              const templateValue = ('template' in datas)?datas.template:''
              domElement.value = templateValue.replace(/%%NEWEDITELINE%%/gm, '\n').replace(/%%NEWLINE%%/g, "<br>");

              // selection de l'élément model
              domElement.addEventListener('mousedown', () => {
                const model = this.editor.model;
                const selectedElement = model.document.selection.getSelectedElement();
    
                if ( selectedElement !== element ) {
                  model.change(writer => writer.setSelection(element, 'on'));
                }
              }, true);

              // ajout écoute modification
              domElement.addEventListener('input', () => {
                const model = this.editor.model;
                const selectedElement = model.document.selection.getSelectedElement();
                model.change(writer => {
                  var template_text = domElement.value.replace(/(\r\n|\n|\r)/gm, "%%NEWEDITELINE%%").replace(/<br ?\/?>/g, "%%NEWLINE%%")
                  writer.setAttribute('template', template_text, selectedElement);
                })
              }, true);
            })
            writer.setAttribute('data-cke-ignore-events', 'true', templateEditor);
            halData.push(templateEditor);
          } else {
            const localEditor = writer.createRawElement('span', {
              class: 'halTagTemplateLocal',
              contenteditable: true,
            }, domElement => {
              domElement.innerText = ('template_local' in datas)?datas.template_local:'fr-FR';
    
              // selection de l'élément model
              domElement.addEventListener('mousedown', () => {
                const model = this.editor.model;
                const selectedElement = model.document.selection.getSelectedElement();
    
                if (selectedElement !== element) {
                  model.change(writer => writer.setSelection(element, 'on'));
                }
              }, true);
  
              domElement.addEventListener('DOMSubtreeModified', () => {
                const model = this.editor.model;
                const selectedElement = model.document.selection.getSelectedElement();
                model.change(writer => {
                  writer.setAttribute('template_local', domElement.innerText, selectedElement);
                })
              }, true);
            });
            writer.setAttribute('data-cke-ignore-events', 'true', localEditor);
            halData.push(writer.createContainerElement('div', {}, [
              writer.createText(t('Localization: ')),
              localEditor
            ]));
            const filenameEditor = writer.createRawElement('span', {
              class: 'halTagTemplateFilename',
              contenteditable: true,
            }, domElement => {
              domElement.innerText = ('template_filename' in datas)?datas.template_filename:'à renseigner';
    
              // selection de l'élément model
              domElement.addEventListener('mousedown', () => {
                const model = this.editor.model;
                const selectedElement = model.document.selection.getSelectedElement();
    
                if (selectedElement !== element) {
                  model.change(writer => writer.setSelection(element, 'on'));
                }
              }, true);
  
              domElement.addEventListener('DOMSubtreeModified', () => {
                const model = this.editor.model;
                const selectedElement = model.document.selection.getSelectedElement();
                model.change(writer => {
                  writer.setAttribute('template_filename', domElement.innerText, selectedElement);
                })
              }, true);
            });
            writer.setAttribute('data-cke-ignore-events', 'true', filenameEditor);
            halData.push(writer.createContainerElement('div', {}, [
              writer.createText(t('CSL filename: ')),
              filenameEditor
            ]));
          }
        }
        return toWidget(writer.createContainerElement('section', {class: 'halTag'}, halData), writer);
      }
    });
  
    // generate data from halTag element.
    conversion.for('dataDowncast').elementToElement( {
      model: 'halTag',
      view: ( element, { writer } ) => {
        var datas = {}
        for(const key of element.getAttributeKeys()) {
          datas[key] = element.getAttribute(key);
        }
        return writer.createText('[hal]' + JSON.stringify(datas) + '[/hal]');
      }
    });
  }
}
