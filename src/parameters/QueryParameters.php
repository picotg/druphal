<?php

/**
 * 
 * Tools to manage query parameters.
 * 
 * @author Gaël PICOT
 * 
 * drupHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

namespace Drupal\druphal\parameters;

use uga\hallib\queryDefinition\DataContainer;
use uga\hallib\queryDefinition\IntervalElement;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\queryDefinition\QueryTreeElement;
use uga\hallib\search\SearchField;
use uga\hallib\search\SearchQuery;

require_once \Drupal::service('extension.list.module')->getPath('druphal').'/vendor/autoload.php';

/**
 * Contain all of query parameter and make query.
 */
class QueryParameters extends DataContainer {

    /**
     * instance
     */
    protected string $instance = '';

    /**
     * start year
     */
    protected int $min_year = 0;

    /**
     * end year
     */
    protected int $max_year = 0;

    /**
     * author query type
     * idhal or classcal
     */
    protected string $authorQueryType = 'classical';

    /**
     * structure query type
     * acronym or classcal
     */
    protected string $structureQueryType = 'acronym';

    /**
     * sort field
     */
    protected string $sort = 'producedDate_s';

    /**
     * sort direction
     */
    protected string $sort_type = 'desc';

    /**
     * number of results
     */
    protected int $rows = 100;

    /**
     * list of returned field
     */
    protected string $result_fields = 'citationFull_s';

    /**
     * doctypes
     */
    protected string $doctypes = '';

    /**
     * authors
     */
    protected string $author = '';

    /**
     * structure
     */
    protected string $structure = '';

    /**
     * Custom query
     */
    protected string $query = '';

    /**
     * render by doctype
     */
    protected bool $renderByDoctype = false;
    
    /**
     * constructeur
     * 
     * @param \stdClass $values values of parameter
     */
    public function __construct(\stdClass $values=null) {
        parent::__construct($values);
        if($this->instance!==''&&substr($this->instance, -1)!=='/') $this->instance .= '/';
    }

    public function getQueryType() {
        if($this->author !== '') {
            return 'author';
        } elseif($this->structure !== '') {
            return 'structure';
        } elseif($this->query !== '') {
            return 'custom';
        } else {
            return 'error';
        }
    }

    /**
     * Make instance of SearchQuery.
     */
    public function makeQuery(): SearchQuery {
        $query = new SearchQuery([
            'rows' => $this->rows,
            'instance' => $this->instance,
            'sort' => SearchField::getVarient($this->sort),
            'ascSortDirection' => $this->sort_type === 'asc',
        ]);
        if($this->queryType === 'custom') {
            $customQuery = $this->addResultField($this->query);
            if(!str_starts_with($customQuery, 'https://api.archives-ouvertes.fr/search/')) {
                $customQuery = 'https://api.archives-ouvertes.fr/search/'.$customQuery;
            }
            $query->customQuery = $customQuery;
        } else {
            $query->addReturnedFields(explode(',', $this->result_fields));
            if($this->min_year!==0||$this->max_year!==0) {
                $query->addFilterQuery(new IntervalElement([
                    'minValue' => $this->min_year!==0?$this->min_year:'*',
                    'maxValue' => $this->max_year!==0?$this->max_year:'*',
                    'field' => SearchField::getVarient('producedDateY_i'),
                ]));
            }
            if($this->doctypes!='') {
                $query->addFilterQuery(new QueryTreeElement([
                    'field' => SearchField::getVarient('docType_s'),
                    'elements' => explode('+OR+', substr($this->doctypes, 1, strlen($this->doctypes) - 2))
                ]));
            }
            $baseQueryParameter = [];
            switch($this->queryType) {
                case 'author':
                    $baseQueryParameter['value'] = $this->parseAndOr($this->author);
                    if ($this->authorQueryType === 'idHal') {
                        if($this->getIdHalInt()) {
                            $baseQueryParameter['field'] = SearchField::getVarient('authIdHal_i');
                        } else {
                            $baseQueryParameter['field'] = SearchField::getVarient('authIdHal_s');
                        }
                    }
                    break;
                case 'structure':
                    $baseQueryParameter['value'] = $this->parseAndOr($this->structure);
                    if ($this->structureQueryType === 'classical') {
                        $baseQueryParameter['field'] = SearchField::getVarient('structure_t');
                    } elseif ($this->structureQueryType === 'acronym') {
                        $baseQueryParameter['field'] = SearchField::getVarient('structAcronym_t');
                    }
                    break;
            }
            $query->baseQuery = new LiteralElement($baseQueryParameter);
        }
        return $query;
    }

    /**
     * Add result field to a query
     */
    public function addResultField($query): string {
        if(strpos($query, '?')) {
            return $query.'&fl='.$this->result_fields;
        } else {
            return $query.'?fl='.$this->result_fields;
        }
    }

    /**
     * replace + by " AND " and , by " OR " 
     */
    public function parseAndOr($value): string {
        if(str_contains('+', $value) || str_contains(',', $value)) {
            $value = str_replace([' + ', ' +', '+ ', '+'], '" AND "', $value);
            $value = str_replace([' , ', ' ,', ', ', ','], '" OR "', $value);
            return '("'.$value.'")';
        }
        else {
            return $value;
        }
    }

    /**
     * return true if idHal is in integer format
     * 
     * @return bool idHal integer format
     */
    public function getIdHalInt():bool {
        $authors = explode(',', $this->author);
        $author0 = explode('+', $authors[0]);
        return intval($author0[0]) > 0;
    }
}
