/**
 * @file defines InsertHalTagCommand, which is executed when one of
 * the HalTag type is pressed in toolbar.
 */

import { Command } from 'ckeditor5/src/core';

export default class insertHalTag extends Command {
  execute(queryMode='author') {
    const { model } = this.editor;
    const { t } = this.editor.locale;

    model.change((writer) => {
      // Insert haltag Element at the current selection position
      // in a way that will result in creating a valid model structure.
      model.insertContent(createHalTag(writer, queryMode, t));
    });
  }

  refresh() {
    const { model } = this.editor;
    const { selection } = model.document;

    // Determine if the cursor (selection) is in a position where adding a
    // simpleBox is permitted. This is based on the schema of the model(s)
    // currently containing the cursor.
    const allowedIn = model.schema.findAllowedParent(
      selection.getFirstPosition(),
      'halTag',
    );

    // If the cursor is not in a location where a simpleBox can be added, return
    // null so the addition doesn't happen.
    this.isEnabled = allowedIn !== null;
  }
}

function createHalTag(writer, queryMode, t) {
  // Create instances of the three elements registered with the editor in
  // haltagediting.js.
  const datas = {
    author: {
      author: t('Author Name'),
      authorQueryType: 'classical',
    },
    structure: {
      structure: t('Structure Name'),
      structureQueryType: 'acronym',
    },
    query: {
      query: 'https://api.archives-ouvertes.fr/search/',
      template: '{% for item in items%}%%NEWEDITELINE%%{{ item.citationFull_s|raw }}%%NEWEDITELINE%%%%NEWLINE%%%%NEWLINE%%%%NEWEDITELINE%%{% endfor %}',
      result_fields: 'citationFull_s'
    }
  }
  const halTag = writer.createElement('halTag', datas[queryMode]);
  return halTag;
}
