import {
    createDropdown,
    addListToDropdown,
    Model,
    SwitchButtonView,
} from 'ckeditor5/src/ui';
import HaltagView from './haltagview';
import { Collection } from 'ckeditor5/src/utils';

/**
 * baloon editor for selecting doctype in Haltag.
 */
export default class HaltagDoctypesView extends HaltagView {
    _doctypes = [];

    constructor(locale, druphalConfig, datas) {
        super(locale, druphalConfig, datas);
        if('doctypes' in this._currentData) {
            this._doctypes = this._currentData['doctypes'].substring(1, this._currentData['doctypes'].length-1).split('+OR+');
        }
    }

    _buildForm() {
        const { t } = this.locale;

        if('doctypes' in this._currentData) {
            this._doctypes = this._currentData['doctypes'].substring(1, this._currentData['doctypes'].length-1).split('+OR+');
        }
        if (this._doctypes === undefined) this._doctypes = [];
        var values = this._druphalConfig.doctypes;
        const field = new createDropdown(this.locale);
        field.panelView.template.attributes.class.push('druphal-dropdown');

        var buttonValue = {
            label: t('Doctypes'),
            withText: true
        }

        if(this._doctypes.length === 0) {
            buttonValue['isEnabled'] = false;
        }

        field.buttonView.set(buttonValue);

        const switchButton = new SwitchButtonView();

        switchButton.set( {
            label: t('all'),
            withText: true,
            isOn: !this._doctypes.length
        } );

        switchButton.on( 'execute', () => {
            switchButton.isOn = !switchButton.isOn;
            field.buttonView.isEnabled = !switchButton.isOn;
            if(switchButton.isOn) {
                this._doctypes = [];
            }
        });

        const items = new Collection();

        for (const valuesKey in values) {
            items.add({
                type: 'switchbutton',
                model: new Model({
                    id: valuesKey,
                    withText: true,
                    label: values[valuesKey].label,
                    isOn: this._doctypes.includes(valuesKey),
                })
            })
        }

        addListToDropdown(field, items);

        this.listenTo(field, 'execute', eventInfo => {
            const source = eventInfo.source;
            source.isOn = !source.isOn;
            if(source.isOn) {
                this._doctypes.push(source.id);
            } else {
                this._doctypes = this._doctypes.filter(value => value != source.id);
            }
        });

        this._buildedForm['doctypes'] = field;
        this._buildedForm['alltypes'] = switchButton;
        return Object.values(this._buildedForm);
    }

    _updateDatas() {
        if (this._doctypes.length !== 0) {
            this._currentData['doctypes'] = '(' + this._doctypes.join('+OR+') + ')';
        } else if ('doctypes' in this._currentData) {
            delete this._currentData.doctypes;
        }
    }
}
