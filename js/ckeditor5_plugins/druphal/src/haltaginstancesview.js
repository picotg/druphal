import {
    createDropdown,
    Model,
    addListToDropdown,
} from 'ckeditor5/src/ui';
import HaltagView from './haltagview';
import { Collection } from 'ckeditor5/src/utils';


/**
 * Contextual balloon editor for choosing an instance over all Hal instances.
 */
export default class HaltaginstancesView extends HaltagView {
    _buildForm() {
        const { t } = this.locale;
        const instanceField = createDropdown(this.locale);
        instanceField.panelView.template.attributes.class.push('druphal-dropdown');

        instanceField.buttonView.set({
            label: t('no instance'),
            withText: true
        })

        const items = new Collection();
        
        items.add({
            type: 'button',
            model: new Model({
                id: 0,
                withText: true,
                label: t('no instance'),
            })
        })

        var values = this._druphalConfig.instances;
        for (const value of values) {
            items.add({
                type: 'button',
                model: new Model({
                    id: value.code,
                    withText: true,
                    label: value.name,
                })
            })
        }

        addListToDropdown(instanceField, items);
        this.listenTo(instanceField, 'execute', eventInfo => {
            var source = eventInfo.source;
            if(source.id === 0) {
                instanceField.buttonView.set({label: t('no instance')});
                delete this._currentData.instance;
            } else {
                instanceField.buttonView.set({label: source.label});
                this._currentData.instance = source.id;
            }
        });

        this._buildedForm['instance'] = instanceField;
        return Object.values(this._buildedForm);
    }
}