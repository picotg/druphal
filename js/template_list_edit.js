(function ($, Drupal, drupalSettings) {
    $(document).ready(function() {
        templateList = JSON.parse($('#template_list_string_value').val());
        $("#drupdhal_template_list").unbind('change');
        $("#drupdhal_template_list").on('change', async function(ev) {
            ev.preventDefault();
            currentValue = $("#drupdhal_template_list").val();
            $("#drupdhal_template_name").val(currentValue);
            if(currentValue!='') {
                var tempaleData = templateList[currentValue];
                $('#delete_template').show();
            } else {
                $('#delete_template').hide();
                var tempaleData = {
                    template_engine: 'CSL',
                    fields: '',
                    template: '',
                    template_local: '',
                    template_filename: ''
                };
            }
            await $('#drupdhal_template_engine_list').val(tempaleData.template_engine);
            await updateDisplay();
            $("#drupdhal_template_fields").val(tempaleData.fields);
            $("#drupdhal_template").val(tempaleData.template);
            $('#drupdhal_template_local').val(tempaleData.template_local);
            $('#drupdhal_template_filename').val(tempaleData.template_filename);
        });
        async function updateDisplay() {
            var value = $('#drupdhal_template_engine_list').val();
            var inverse = (value!=='CSL')?'CSL':'twig';
            await $('.' + value).show();
            await $('.' + inverse).hide();
        };
        $('#drupdhal_template_name').on('input', function(e) {
            if($('#drupdhal_template_name').val() in templateList) {
                $("#drupdhal_template_list").val($('#drupdhal_template_name').val()).trigger('change');
            } else {
                $("#drupdhal_template_list").val('');
                $('#delete_template').hide();
            }
        })
        $('#delete_template').hide();
        $('#drupdhal_template_engine_list').unbind('change');
        $('#drupdhal_template_engine_list').on('change', updateDisplay);
        $('#delete_template').unbind('click');
        $('#delete_template').click(ev => {
            ev.preventDefault();
            delete templateList[$("#drupdhal_template_list").val()];
            $('#template_list_string_value').val(JSON.stringify(templateList));
            $('#drupdhal_template_list').find('option:selected').remove();
            $('#drupdhal_template_list').val('').trigger('change');
        })
        $('#reinit_template').unbind('click');
        $('#reinit_template').click(ev => {
            ev.preventDefault();
            templateList = JSON.parse(drupalSettings.druphal.template_list);
            $('#template_list_string_value').val(JSON.stringify(templateList));
            $('#drupdhal_template_list').find('option').remove();
            $('#drupdhal_template_list').append($('<option>', {
                value: '',
                text: 'nouveau'
            }));
            for(template in templateList) {
                $('#drupdhal_template_list').append($('<option>', {
                    value: template,
                    text: template
                }));
            }
            $('#drupdhal_template_list').val('').trigger('change');
        })
        $('#valid_template_mod').unbind('click');
        $('#valid_template_mod').click(ev => {
            ev.preventDefault();
            var template_engine = $('#drupdhal_template_engine_list').val();
            var template_name = $("#drupdhal_template_name").val();
            if(templateList[template_name] === undefined) {
                $('#drupdhal_template_list').append($('<option>', {
                    value: template_name,
                    text: template_name
                }));
            }
            templateList[template_name] = {}
            templateList[template_name]['template_engine'] = template_engine;
            if(template_engine === 'CSL') {
                templateList[template_name]['template_local'] = $('#drupdhal_template_local').val();
                templateList[template_name]['template_filename'] = $('#drupdhal_template_filename').val();
            } else {
                templateList[template_name]['fields'] = $("#drupdhal_template_fields").val();
                templateList[template_name]['template'] = $("#drupdhal_template").val();
            }
            $('#template_list_string_value').val(JSON.stringify(templateList));
            $('#drupdhal_template_list').val('').trigger('change');
        })
    })
})(jQuery, Drupal, drupalSettings);