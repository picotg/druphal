<?php

namespace Drupal\Tests\druphal\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

use Drupal\druphal\renderEngine\RenderEngine;

class FakeListModule {
    public function getPath(string $_):string {
        return dirname(__FILE__, 4);
    }
}

/**
 * @group druphal
 */
class HallibUnitCase extends UnitTestCase {
    private const QI_URL_API = 'https://api.archives-ouvertes.fr/search/saga/?q=structure_t:3SR&rows=10&fl=citationFull_s&sort=producedDate_s+desc';
    public function setup(): void {
        parent::setUp();

        $container = new ContainerBuilder();
        \Drupal::setContainer($container);
        \Drupal::getContainer()->set('extension.list.module', new FakeListModule());
    }

    public function testURLOnly() {
        $queryParameter = json_decode('{"structure":"3SR","structureQueryType":"classical","rows":10,"selected_template":"American Psychological Association (APA)","template_engine":"URLOnly","template_filename":"apa-6th-edition","template_local":"fr-FR","instance":"saga"}');
        $renderEngine = RenderEngine::createRenderEngine($queryParameter);
        $this->assertEquals($renderEngine->render(), 'SearchQuery = https://api.archives-ouvertes.fr/search/saga/?q=structure_t%3A3SR&wt=json&rows=10&fl=citationFull_s&sort=producedDate_s+desc');
    }

    public function testQueryIteratorI() {
        RenderEngine::addEngine('QI', ResultRenderEngine::class);
        $queryParameter = json_decode('{"structure":"3SR","structureQueryType":"classical","rows":10,"selected_template":"American Psychological Association (APA)","template_engine":"QI","template_filename":"apa-6th-edition","template_local":"fr-FR","instance":"saga"}');
        $renderEngine = RenderEngine::createRenderEngine($queryParameter);
        $this->assertInstanceOf(ResultRenderEngine::class, $renderEngine);
        $dataQI = json_decode(file_get_contents(static::QI_URL_API));
        $this->assertTrue(isset($dataQI->response) && isset($dataQI->response->docs));
        $docs = $dataQI->response->docs;
        $index = 0;
        foreach($renderEngine->getQueryIterator() as $doc) {
            $this->assertEquals($doc->citationFull_s, $docs[$index]->citationFull_s);
            $index += 1;
        }
    }
}
