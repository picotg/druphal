<?php
/**
 * 
 * 
 * Hal instances and doctypes list Update management.
 * 
 * @author Gaël PICOT
 * 
 * drupHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 * 
 */
namespace Drupal\druphal\DataManager;

/**
 * Hal instances and doctypes list Update manager.
 */
class UpdateHalDataListe {

    /**
     * Update infos
     */
    const INFOS = [
        'instance' => [
            'url' => 'https://api.archives-ouvertes.fr/ref/instance',
            'file_name' => 'instance_list',
            'backup_file_name' => 'instance_list_bakup',
            'message_update' => 'mise à jour des instances',
            'message_noupdate' => 'pas de mise à jour des instances',
            'message_try' => 'verification mise à jour disponible des instances',
            'update_timestamp' => 'update_instance',
            'message_restaure' => 'l\'ancienne liste des instance à était restaurer',
            'message_restaure_error' => 'une tentative de restauration de la liste des instances à échoué',
            'try_update_timestamp' => 'try_update_instance',
        ],
        'doctype' => [
            'url' => 'https://api.archives-ouvertes.fr/ref/doctype',
            'file_name' => 'doctype_list',
            'backup_file_name' => 'doctype_list_bakup',
            'message_update' => 'mise à jour des type de document',
            'message_noupdate' => 'pas de mise à jour des type de document',
            'message_try' => 'verification mise à jour disponible des type de document',
            'update_timestamp' => 'update_doctype',
            'message_restaure' => 'l\'ancienne liste des types de document à était restaurer',
            'message_restaure_error' => 'une tentative de restauration de la liste des types de document à échoué',
            'try_update_timestamp' => 'try_update_doctype',
        ],
    ];

    /**
     * type of updater
     */
    private string $type = 'doctype';

    /**
     * constructeur
     * 
     * @param string $type type of updater
     */
    public function __construct(string $type) {
        if(key_exists($type, $this::INFOS)) {
            $this->type = $type;
        }
    }

    public static function name_cmp($a, $b) {
        return strcmp($a["name"], $b["name"]);
    }

    public static function create_sorted_instance() {
        $settings = \Drupal::configFactory()->getEditable('druphal.settings');
        $sorted_instance_data = [];
        $instanceData = json_decode(\Drupal::configFactory()->get('druphal.settings')->get('instance_list'));
        $sorted_instance_data = [];
        foreach($instanceData->response->docs as $instance) {
            array_push($sorted_instance_data, ['code' => $instance->code, 'name' => $instance->name]);
        }
        usort($sorted_instance_data, [static::class, 'name_cmp']);
        $settings->set('sorted_instance_data', json_encode($sorted_instance_data))->save();
    }

    /**
     * Update data.
     */
    public function update() {
        $settings = \Drupal::configFactory()->getEditable('druphal.settings');
        \Drupal::logger('DrupHal')->notice($this::INFOS[$this->type]['message_try']);
        $actualContent = $settings->get($this::INFOS[$this->type]['file_name']);
        $updatedContent = file_get_contents($this::INFOS[$this->type]['url']);
        $settings->set($this::INFOS[$this->type]['try_update_timestamp'], time())->save();
        if(strcmp($actualContent, $updatedContent) === 0 || $settings->get($this::INFOS[$this->type]['backup_file_name']) === '') {
            \Drupal::logger('DrupHal')->notice($this::INFOS[$this->type]['message_noupdate']);
        } else {
            if($this->check($updatedContent)) {
                $settings->set($this::INFOS[$this->type]['file_name'], $updatedContent)->save();
                $settings->set($this::INFOS[$this->type]['backup_file_name'], $actualContent)->save();
                $settings->set($this::INFOS[$this->type]['update_timestamp'], time())->save();
                \Drupal::logger('DrupHal')->notice($this::INFOS[$this->type]['message_update']);
                if ($this->type === 'instance') {
                    UpdateHalDataListe::create_sorted_instance();
                }
            } else {
                \Drupal::logger('DrupHal')->notice($this::INFOS[$this->type]['message_noupdate']);
            }
        }
    }

    /**
     * restaure data.
     */
    public function restaure() {
        $settings = \Drupal::configFactory()->getEditable('druphal.settings');
        if($settings->get($this::INFOS[$this->type]['backup_file_name']) !== '') {
            $oldContent = $settings->get($this::INFOS[$this->type]['backup_file_name']);
            $settings->set($this::INFOS[$this->type]['file_name'], $oldContent)->save();
            $settings->set($this::INFOS[$this->type]['backup_file_name'], '')->save();
        }
    }

    /**
     * Check downloaded data.
     * 
     * @return bool $data data to check
     */
    public function check($data):bool {
        $data = json_decode($data);
        if($this->type==="doctype"&&count($data->response->result->doc) > 0) {
            return true;
        } elseif($this->type==="instance"&&$data->response->numFound > 0) {
            return true;
        }
        return false;
    }
}
