(function ($, Drupal, CKEDITOR) {
    CKEDITOR.dialog.add('druphalDialog', function (editor) {
        var allYears = [];
        var default_value = new ParameterValue();
        for(var i = new Date().getFullYear(); i > 1970 ; i--) {
            allYears.push([i, i]);
        }
        var templateList = JSON.parse(editor.config.template_list);
        var templateListItems = [];
        for(const templateName in templateList) {
            templateListItems.push([templateName, templateName])
        }
        var commonElements = [
            {
                type:           'select',
                label:          'Type(s) de document',
                id:             'doctypes',
                items:          [['tous', '']].concat(JSON.parse(editor.config.doctypes)),
                className:      'doctypes',
                multiple:       true,
                size:           4,
                style:          "height: 100%;",
                default:        default_value.doctypes,
            },
            {
                type: 'hbox',
                children: [
                    {
                        type:           'select',
                        label:          'Année de début',
                        id:             'min_year',
                        className:      'min_year',
                        items:          [['pas de date', '0']].concat(allYears),
                        default:        default_value.min_year
                    },
                    {
                        type:           'select',
                        label:          'Année de fin',
                        id:             'max_year',
                        className:      'max_year',
                        items:          [['pas de date', '0']].concat(allYears),
                        default:        default_value.max_year
                    }
                ]
            },
            {
                type:           'text',
                label:          'Nombre de publications à afficher',
                id:             'rows',
                className:      'rows',
                default:        default_value.rows
            },
            {
                type:'hbox',
                widths: ['75%', '25%'],
                children: [
                    {
                        type:           'select',
                        label:          'Critère de tri',
                        id:             'sort',
                        className:      'sort',
                        items:          [
                            ['date de production', 'producedDate_s'],
                            ['titre', 'title_sort'],
                        ],
                        default:        default_value.sort
                    },
                    {
                        type:           'select',
                        label:          'Ordre de tri',
                        id:             'sort_type',
                        className:      'sort_type',
                        items:          [
                            ['ascendant', 'asc'],
                            ['descendant', 'desc'],
                        ],
                        default:        default_value.sort_type
                    }
                ]
            },
            {
                type:           'checkbox',
                id:             'renderByDoctype',
                className:      'renderByDoctype',
                label:          'Grouper par types de documents',
            },
            {
                type:           'select',
                label:          'Style de mise en forme',
                id:             'selected_template',
                items:          [['Style Hal par défaut', '']].concat(templateListItems),
                className:      'selected_template',
                default:        '',
            },
        ];
        authorElement = [...commonElements]
        if(editor.config.default_instance !== '') {
            defaultInstance = JSON.parse(editor.config.default_instance);
            authorElement.splice(1, 0, 
                {
                    type:           'select',
                    label:          'portail',
                    id:             'instance',
                    items:          [['portail Hal national', ''], defaultInstance],
                    className:      'instance',
                    default:        defaultInstance[1],
                },
            )
        }
        return {
            currentValue: null,
            title: 'Sélectionnez les publications Hal',
            resizable:      CKEDITOR.DIALOG_RESIZE_BOTH,
            minWidth:       500,
            minHeight:      400,
            contents: [{
                id: 'druphalAuthor',
                label:      'Auteur(s)/Autrice(s)',
                title:      'Requête par Auteur(s)/Autrice(s)',
                accessKey:  'A',
                elements: [
                    {
                        type: 'hbox',
						widths: [ '25%', '75%' ],
                        children: [
                            {
                                type:           'select',
                                label:          'Type de recherche',
                                id:             'authorQueryType',
                                className:      'authorQueryType',
                                items: [
                                    ['prénom nom', 'classical'],
                                    ['idHal', 'idHal'],
                                ],
                                default:        default_value.authorQueryType
                            },
                            {
                                type:           'text',
                                label:          'Auteur(s)/Autrice(s)',
                                id:             'author',
                                className:      'author',
                                default:        default_value.author
                            }
                        ]
                }
                ].concat(authorElement)
            },{
                id: 'druphalStructure',
                label:      'Structure(s)',
                title:      'Requête par structure',
                accessKey:  'S',
                elements: [
                    {
                        type: 'hbox',
                        widths: [ '25%', '75%' ],
                        children: [
                            {
                                type:           'select',
                                label:          'Type de recherche',
                                id:             'structureQueryType',
                                className:      'structureQueryType',
                                items: [
                                    ['nom cours (acronyme)', 'acronym'],
                                    ['nom long', 'classical'],
                                ],
                                default:        default_value.structureQueryType
                            },
                            {
                                type:           'text',
                                label:          'Structure(s)',
                                id:             'structure',
                                className:      'structure',
                                default:        default_value.structure
                            }
                        ]
                    }
                ].concat(authorElement)
            },{
                id: 'druphalCustom',
                label:      'Avancé',
                title:      'Avancé',
                accessKey:  'P',
                elements: [
                    {
                        type:           'text',
                        label:          'Requête personnalisée',
                        id:             'query',
                        className:      'query',
                        default:        default_value.query
                    },
                    {
                        type:           'select',
                        label:          'Moteur de template',
                        items:          [
                            ['twig', 'twig'],
                            ['Citation Style Language', 'CSL']
                        ],
                        id:             'selectEngine',
                        className:      'selectEngine',
                        default:        'twig',
                    },
                    {
                        id: 'twigParams',
                        type: 'vbox',
                        className: 'twigParams',
                        children: [
                            {
                                type:           'text',
                                label:          'Champs de retour',
                                id:             'result_fields',
                                className:      'result_fields',
                                default:        default_value.result_fields
                            },
                            {
                                type:           'textarea',
                                label:          'Template Twig',
                                id:             'template',
                                className:      'template',
                                default:        default_value.editor_template
                            }
                        ]
                    },
                    {
                        id: 'CSLParams',
                        type: 'vbox',
                        className: 'CSLParams',
                        children: [
                            {
                                type:           'text',
                                label:          'Localisation',
                                id:             'template_local',
                                className:      'template_local',
                                default:        default_value.template_local
                            },
                            {
                                type:           'text',
                                label:          'Nom du fichier template CSL',
                                id:             'template_filename',
                                className:      'template_filename',
                                default:        default_value.template_filename
                            }
                        ]
                    }
                ]
            }],
            onOk: function() {
                var jsonData = {}
                var currentTab = this._.currentTabId;
                if(currentTab === 'druphalAuthor') {
                    jsonData.author = this.getValueOf('druphalAuthor', 'author');
                    if(jsonData.author === '') return;
                    jsonData.authorQueryType = this.getValueOf('druphalAuthor', 'authorQueryType');
                } else if (currentTab === 'druphalStructure') {
                    jsonData.structure = this.getValueOf('druphalStructure', 'structure');
                    if(jsonData.structure === '') return;
                    jsonData.structureQueryType = this.getValueOf('druphalStructure', 'structureQueryType');
                } else if (currentTab === 'druphalCustom') {
                    jsonData.query = this.getValueOf('druphalCustom', 'query');
                    if(jsonData.query === '') return;
                }
                if(currentTab !== 'druphalCustom') {
                    var selectDoctypes = $('#'+this.getContentElement(currentTab, 'doctypes')._.inputId).val();
                    if(!selectDoctypes.includes('')&&selectDoctypes.length!=0) {
                        jsonData.doctypes = '(' + selectDoctypes.join('+OR+') + ')';
                    }
                    if(this.getContentElement(currentTab, 'instance') !== undefined && this.getValueOf(currentTab, 'instance') !== '') {
                        jsonData.instance = this.getValueOf(currentTab, 'instance');
                    }
                    if(this.getValueOf(currentTab, 'sort')!='producedDate_s') {
                        jsonData.sort = this.getValueOf(currentTab, 'sort');
                    }
                    if(this.getValueOf(currentTab, 'sort_type')!='desc') {
                        jsonData.sort_type = this.getValueOf(currentTab, 'sort_type');
                    }
                    var min_year = parseInt(this.getValueOf(currentTab, 'min_year'));
                    var max_year = parseInt(this.getValueOf(currentTab, 'max_year'));
                    if(max_year<min_year&&max_year!=0) {
                        alert('l\'année de début est suppérieur à l\'année de fin. Les années n\'ont donc pas été rajouter');
                    } else {
                        if (Number.isInteger(min_year)&&min_year!=0) jsonData.min_year = min_year;
                        if (Number.isInteger(max_year)&&max_year!=0) jsonData.max_year = max_year;
                    }
                    if(this.getValueOf(currentTab,'renderByDoctype')) {
                        jsonData.renderByDoctype = true;
                    }
                    var rows = parseInt(this.getValueOf(currentTab,'rows'));
                    if(Number.isInteger(rows)&&rows!=100&&rows>0) jsonData.rows = rows;
                    selected_template = this.getValueOf(currentTab, 'selected_template');
                    if(selected_template !== '') {
                        jsonData.selected_template = selected_template;
                        if(templateList[selected_template].template_engine === 'twig') {
                            if(templateList[selected_template].fields !== 'citationFull_s') {
                                jsonData.result_fields = templateList[selected_template].fields;
                            }
                            if(templateList[selected_template].template != '') {
                                jsonData.template = templateList[selected_template].template
                                    .replace(/(\r\n|\n|\r)/gm, "%%NEWEDITELINE%%")
                                    .replace(/<br ?\/?>/g, "%%NEWLINE%%");
                            }
                        } else if(templateList[selected_template].template_engine==='CSL') {
                            if(templateList[selected_template].template_filename!='') {
                                jsonData.template_engine = 'CSL';
                                jsonData.template_filename = templateList[selected_template].template_filename;
                                jsonData.template_local = templateList[selected_template].template_local;
                                delete jsonData.result_fields;
                            }
                        }
                    }
                } else {
                    if(this.getValueOf(currentTab, 'selectEngine') === 'twig') {
                        if(this.getValueOf(currentTab, 'result_fields') !== 'citationFull_s') {
                            jsonData.result_fields = this.getValueOf(currentTab, 'result_fields');
                        }
                        if(this.getValueOf(currentTab, 'template') !== '') {
                            jsonData.template = this.getValueOf(currentTab, 'template')
                                .replace(/(\r\n|\n|\r)/gm, "")
                                .replace(/<br ?\/?>/g, "%%NEWLINE%%");
                        }
                    } else if(this.getValueOf(currentTab, 'selectEngine') === 'CSL') {
                        if(this.getValueOf(currentTab, 'template_filename') !== '') {
                            jsonData.template_engine = 'CSL';
                            jsonData.template_filename = this.getValueOf(currentTab, 'template_filename');
                            jsonData.template_local = this.getValueOf(currentTab, 'template_local');
                            delete jsonData.result_fields;
                        }
                    }
                }
                var requestTag = '[hal]' + JSON.stringify(jsonData) + '[/hal]';
                if(this.currentValue.range === null) {
                    editor.insertHtml(requestTag);
                } else {
                    var rangeText = this.currentValue.range.startContainer.getText();
                    this.currentValue.range.startContainer.setText(rangeText.replace(this.currentValue.currentText, requestTag))
                }
            },
            onLoad: async function() {
                async function displayEngine(dialog) {
                    var value = dialog.getCurrent().getValueOf('druphalCustom', 'selectEngine');
                    var inverse = (value!=='CSL')?'CSL':'twig';
                    await $('.' + value + 'Params').show();
                    await $('.' + inverse + 'Params').hide();
                }
                $('.selectEngine').change(function() {
                    displayEngine(CKEDITOR.dialog);
                })
            },
            onShow: async function() {
                // hide template parameter
                $('.CSLParams').hide();

                // get instance list
                var editor = this.getParentEditor();

                // complete data with current selection
                var selections = editor.getSelection().getRanges();
                this.currentValue = new ParameterValue();
                var currentFound = false;
                for (const selection of selections) {
                    if(this.currentValue.getCurrentValue(selection)) {
                        currentFound = true;
                        break;
                    }
                }
                if(currentFound) {
                    this.selectPage(this.currentValue.page);
                    $('.selectEngine select').val(this.currentValue.template_engine);
                    $('.selectEngine select').trigger('change');
                    for(const valueName in this.currentValue) {
                        if(['min_year', 'max_year', 'sort', 'sort_type', 'authorQueryType', 'instance', 'structureQueryType', 'selected_template'].includes(valueName)) {
                            $('.'+valueName +' select').val(this.currentValue[valueName]);
                        } else if(valueName === 'template') {
                            $('.template textarea').val(this.currentValue[valueName]);
                        } else if (valueName === 'doctypes') {
                            var values = this.currentValue[valueName]
                            var listValue = values.substring(1, values.length-1).split('+OR+');
                            $('.doctypes select').val(listValue);
                        } else if (valueName === 'renderByDoctype') {
                            $('.'+valueName +' input').prop( "checked", this.currentValue[valueName]);
                        } else {
                            $('.'+valueName +' input').val(this.currentValue[valueName]);
                        }
                    }
                }
            }
        }
    });
    CKEDITOR.plugins.add('druphal', {
        icons: 'druphal',
        init: function (editor) {
            // Define the editor command that inserts a timestamp.
            editor.addCommand('druphal', new CKEDITOR.dialogCommand('druphalDialog'));
            // Create the toolbar button that executes the above command.
            editor.ui.addButton('druphal', {
                label: 'druphal',
                command: 'druphal'
            });
          }
    })
})(jQuery, Drupal, CKEDITOR);