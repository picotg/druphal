import {
    View,
    ButtonView,
} from 'ckeditor5/src/ui';
import { icons } from 'ckeditor5/src/core';


/**
 * base class for contextual balloon options.
 */
export default class HaltagView extends View {
    _currentData = {};
    _buildedForm = {};
    _druphalConfig = null;
    _mode = 'author';

    get otherBalloon () {
        return null;
    }

    constructor(locale, druphalConfig, datas) {
        super(locale);
        console.log(druphalConfig);
        this._druphalConfig = druphalConfig;
        if('author' in datas) {
            this._mode = 'author'
        } else if('structure' in datas) {
            this._mode = 'structure';
        } else {
            this._mode = 'query';
        }

        const { t } = locale;

        this.saveButtonView = new ButtonView();

        this.saveButtonView.set({
            label: t('save'),
            icon: icons.check,
        })

        this.cancelButtonView = new ButtonView();

        this.cancelButtonView.set({
            label: t('cancel'),
            icon: icons.cancel,
        })

        this.saveButtonView.delegate('execute').to(this, 'save');
        this.cancelButtonView.delegate('execute').to(this, 'cancel');

        this._currentData = datas;

        this.childViews = this.createCollection(
            this._buildForm().concat([
                this.saveButtonView,
                this.cancelButtonView
            ])
        );

        this.setTemplate({
            tag: 'form',
            attributes: {
                class: [
                    'ck',
                    'ck-responsive-form',
                    'druphal-form',
                ],
                tabindex: '-1'
            },
            children: this.childViews
        });
    }

    _buildForm() { }

    _updateDatas() { }

    get datas() {
        this._updateDatas();
        return this._currentData;
    }
}
