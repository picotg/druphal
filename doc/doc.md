# drupHal

## Table des matières

 * [Présentation](#pres)
 * [Add filter and CkEditor plugin](#filter-conf)
 * [Utilisation du plug-in CkEditor 5](#use-plugin5)
 * [Paramètres des requêtes](#qery-parameter)
 * [Formulaire de gestions de configuration des templates](#template-parameter)
 * [Templates](#template)

## Présentation {#pres}

DrupHal est un module permétant d'affiché les résultats d'une requête dans Hal.

### Contenue

Ce module contient:

 * le filtre Hal: remplace les balise \[hal\](...)\[/hal\] par le résultat de la requête Hal correspondante,
 * le plugin CkEditor: permetant d'ajouter et d'éditer visuelement les balises \[hal\](...)\[/hal\],
 * le formulaire d'ajout et de modification de la liste des templates d'affichage préenregistrer,

## Ajout du filtre et plug-in CkEditor {#filter-conf}

dans Configuration > Rédaction de contenu > Formats de texte et éditeurs, entrez dans la configuration
du format de texte voulue.

Pour activer le plugin CkEditor, il suffit d'ajouté l'icone de DrupHal (logo de Hal en noire et blanc)
dans la bar d'outil de votre choix.

Pour activer le filtre Hal il suffit de cocher la case filtre Hal dans la liste des filtres activés.

Options plugin CkEditor :

 * instance par défaut : instance par défaut, si aucun n'est slecetionner, l'option n'aparé plus,
 * Mise à jour manuelle des données : Permet de télécharcher les dernière version de la liste des instances et type de document à partir du serveur de Hal.
 * Restauration manuelle des données : Permet de restauré la précédente version des données,
 
Une tache cron permet d'automatisé la verification des mise à jour. Si le plugin ultimate_cron est installé, elle est executé tout les [dimanche soir à minuit](https://crontab.guru/#0_0_*_*_0) par défaut. Un encadré ajoute la date dernière mise à jour et dernière verification.

## Utilisation du plug-in CkEditor 5 {#use-plugin5}

### Ajout un affichage

<figure>
<img src="{{doc_path}}img/ck5/dropdown_menu_fr.png" alt="Ajout un affichage" />
<figcaption aria-hidden="true">Ajout un affichage</figcaption>
</figure>

L'icon DrupHal permet d'ajouter un affichage de publication issu d'une requête Hal.
Il est possible d'ajouter laffichage pour une requête de type auteur/autrice, structure ou personnalisé.

#### Widget d'édition

Un widget spécifique permet de modifier les paramétres de la requête et de l'affichage.
De manière général les texte surligner en vert indique un text qu'il permet d'ouvrir une bulle d'option en cliquant dessus.

### Affichage des publication Hal pour un auteur/une autrice

<figure>
<img src="{{doc_path}}img/ck5/author_general_fr.png" alt="Affichage des publication pour un auteur/une autrice" />
<figcaption aria-hidden="true">Affichage des publication pour un auteur/une autrice</figcaption>
</figure>

#### Choix du type de requête

<figure>
<img src="{{doc_path}}img/ck5/author_choose_type_fr.png" alt="Choix du type de requête" />
<figcaption aria-hidden="true">Choix du type de requête</figcaption>
</figure>

Permet de choisir entre une recherche de type nom prenom ou une recherche sur un idHal. Celà peut être un idHal numérique ou sous forme de chaîne de caractère (le plus souvant prenom-nom).

#### Choix des types de documents

<figure>
<img src="{{doc_path}}img/ck5/author_doctypes_fr.png" alt="Choix des types de documents" />
<figcaption aria-hidden="true">Choix des types de documents</figcaption>
</figure>

Permet de choisir les types de documents.

#### Activer le portail par défaut

<figure>
<img src="{{doc_path}}img/ck5/author_instances_fr.png" alt="Activer le portail par défaut" />
<figcaption aria-hidden="true">Activer le portail par défaut</figcaption>
</figure>

Permet d'activer ou désactiver la recherche sur le portail Hal par défaut configurer dans le format de texte.
Il est aussi possible de selectionner un autre portail.

#### Séléction d'un portail parmis la liste compléte

<figure>
<img src="{{doc_path}}img/ck5/author_all_instances_fr.png" alt="Séléction d'un portail parmis la liste compléte" />
<figcaption aria-hidden="true">Séléction d'un portail parmis la liste compléte</figcaption>
</figure>

Permet de selectionner un autre portail.
Interface par défaut si aucun portail par défaut n'est séléctionné.

#### Séléction des dates

<figure>
<img src="{{doc_path}}img/ck5/author_dates_fr.png" alt="Séléction des dates" />
<figcaption aria-hidden="true">Séléction des dates</figcaption>
</figure>

Permet de selectionner les dates de début et de fin de publication du document ([champs Hal producedDateY_i](https://api.archives-ouvertes.fr/docs/search/?schema=fields#go_producedDateY_i)).

#### Choix nombres de résultats

<figure>
<img src="{{doc_path}}img/ck5/author_dates_fr.png" alt="Choix nombres de résultats" />
<figcaption aria-hidden="true">Choix nombres de résultats</figcaption>
</figure>

Choix du nombre de résultats renvoyer par la requête ([champs row de la requête](https://api.archives-ouvertes.fr/docs/search/?#rows)).

#### Options de tri

<figure>
<img src="{{doc_path}}img/ck5/author_sort_fr.png" alt="Options de tri" />
<figcaption aria-hidden="true">Options de tri</figcaption>
</figure>

Permet de choisir le champs de tri et la direction.

#### Option de rendu

<figure>
<img src="{{doc_path}}img/ck5/author_render_option_fr.png" alt="Option de rendu" />
<figcaption aria-hidden="true">Option de rendu</figcaption>
</figure>

Permet de choisir un des template préconfigurer et l'opton de tri par type de document.

### Affichage des publication Hal pour une structure

<figure>
<img src="{{doc_path}}img/ck5/structure_general_fr.png" alt="Affichage des publication pour une structure" />
<figcaption aria-hidden="true">Affichage des publication pour une structure</figcaption>
</figure>

#### Choix type de recherche

<figure>
<img src="{{doc_path}}img/ck5/structure_choose_type_fr.png" alt="Choix type de recherche" />
<figcaption aria-hidden="true">Choix type de recherche</figcaption>
</figure>

Permet de choisir entre une recherche sur le nom d'une ou sur l'acronyme d'une structure.

Les autres options sont identique à un affichage pour auteur/autrice.

### Affichage des publication Hal pour une requête personnalisé

<figure>
<img src="{{doc_path}}img/ck5/custom_general_fr.png" alt="Affichage des publication pour une requête personnalisé" />
<figcaption aria-hidden="true">Affichage des publication pour une requête personnalisé</figcaption>
</figure>

#### Choix du moteur de rendu

<figure>
<img src="{{doc_path}}img/ck5/custom_engine_fr.png" alt="Choix du moteur de rendu" />
<figcaption aria-hidden="true">Choix du moteur de rendu</figcaption>
</figure>

Permet de choisir le moteur de rendu CSL ou Twig.

#### Twig

##### Choix des Champs résultats

<figure>
<img src="{{doc_path}}img/ck5/custom_result_field_fr.png" alt="Choix des Champs résultats" />
<figcaption aria-hidden="true">Choix des Champs résultats</figcaption>
</figure>

Permet de lister les champs résultat de la requête en les séparent par une virgule.
La liste des champs est disponible sur [la documentation officiel de l'API Hal](https://api.archives-ouvertes.fr/docs/search/?schema=fields#fields)

##### Edition du template Twig

<figure>
<img src="{{doc_path}}img/ck5/custom_template_fr.png" alt="Edition du template Twig" />
<figcaption aria-hidden="true">Edition du template Twig</figcaption>
</figure>

Permet d'édité le template Twig.

#### CSL

##### Choix localisation

<figure>
<img src="{{doc_path}}img/ck5/custom_localization_fr.png" alt="Choix localisation" />
<figcaption aria-hidden="true">Choix localisation</figcaption>
</figure>

La localisation renvoi à un fichier disonible sur [le dépot dédier au CSL](https://github.com/citation-style-language/locales).
Il doit être écrit au format standart langue-pays. Par exemple, pour la france, fr-FR ou les états-units : en-US.

##### Choix fichier CSL

<figure>
<img src="{{doc_path}}img/ck5/custom_csl_file_fr.png" alt="Choix fichier CSL" />
<figcaption aria-hidden="true">Choix fichier CSL</figcaption>
</figure>

La liste des fichiers templates disponiblece trouve sur [ce dépot](https://github.com/citation-style-language/styles).
Vous devez préciser le nom d'un des fichier existant.

## Paramètres des requêtes {#qery-parameter}

Les Paramètres des requête Hal sont enregistrer sous la forme d'une chaine JSON présent entre les balise \[hal\](...)\[/hal\].

Les Paramètres non présent, on leurs valeurs par défault.

Seulement un des trois champs de requête (author, structure ou query) doit être renseigné.x

## Formulaire de gestions de configuration des templates {#template-parameter}

Accessible dans la configuration du plugin CkEditor, ce formulaire permet d'ajouter, de supprimer et de modifier les templates préconfigurer pour un format de texte. Il est aussi possible de réinitialiser la liste par défault.

En selectionnant une préconfiguration de template, vous pouvez la supprimer ou la modifier. Si vous modifier le nom, alors cela en créra un nouveau ou selectionnera celui correspondant.

Le bouton de réinitialisation permet de remetre la liste de template par défaut.

Le bouton "Enregistrer la configuration" permet ou soit de créé une nouvelle préconfiguration ou soit de modifier celle en cours.

### Template {#template}

Pour personnaliser le rendu, on peut utiliser l'un des moteurs de rendu. Les options disponible dépende du moteur de rendu choisi.

Les moteurs de rendu disponible sont :

* Citation Style Language (CSL)
* Twig

#### Citation Style Language

Pour rendre les feuilles de style CSL, druphal utilise [citeproc-php](https://github.com/seboettg/citeproc-php). Seul les fauilles de styles prèinstallé peuvent être utiliser.

##### localisation

La localisation renvoi à un fichier disonible sur [le dépot dédier au CSL](https://github.com/citation-style-language/locales).
Il doit être écrit au format standart langue-pays. Par exemple, pour la france, fr-FR ou les états-units : en-US

##### nom de fichiers template

La liste des fichiers templates disponiblece trouve sur [ce dépot](https://github.com/citation-style-language/styles).
Vous devez préciser le nom d'un des fichier existant.

#### Twig

##### Champs de retour (result_fields)

Champs Hal ([liste compléte](https://api.archives-ouvertes.fr/docs/search/?schema=fields#fields)) retourner par la requête.

Les champs doivent être séparé par des virgule sans espace.

##### Template

Template Twig de mise en forme du résultat de la requête.

La documentation de Twig est disponible [ici](https://twig.symfony.com/doc/3.x/).

Le Template reçoit en paramètre le tableau items contenant tous les résultats avec les champs renvoyer par la requête (soit défini par le paramètre champs ou le champ fl de la requête personnalisé).

Les champs Hal de type multivalué sont retourné sous forme de tableau.

Exemples de Template :

Avec les champs citationFull_s,authFullName_s pour l'affichage des co-auteur·rice·s.

```
{% for item in items%}{{ item.citationFull_s|raw }}<br /><br />
Co-auteur·rice·s :
{% for authFullName_s in item.authFullName_s%}
{% if not loop.first %}, {% endif %}{{ authFullName_s }}
{% endfor %}<br /><br />
{% endfor %}
```

Avec les champs citationFull_s,abstract_s pour l'affichage de la première version de l'abstract.

```
{% for item in items%}{{ item.citationFull_s|raw }}<br /><br />
{{ item.abstract_s|first }}<br /><br />
{% endfor %}
```
