<?php

/**
 * 
 * Filter to interprate Druphal tag ([hal][/hal]).
 * 
 * @author Gaël PICOT
 * 
 * drupHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 * 
 * @file
 * Contains Drupal\celebrate\Plugin\Filter\FilterCelebrate
 * 
 */

namespace Drupal\druphal\Plugin\Filter;

use Drupal\druphal\renderEngine\RenderEngine;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

const HAL_REGEX = '/\[hal\](.*?)\[\/hal\]/';

/**
 * Provides a filter to interprate Druphal tag ([hal][/hal]).
 *
 * @Filter(
 *   id = "filter_hal",
 *   title = @Translation("Hal filter"),
 *   description = @Translation("Replace Hal markup ([hal]...[/hal]) by render text from Hal query."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterHal extends FilterBase {
    /**
     * {@inheritdoc}
     */
    public function process($text, $langcode) {
        $matches = [];
        $new_text = $text;
        if(preg_match_all(HAL_REGEX, $text, $matches)) {
            foreach($matches[0] as $index => $singleMatch) {
                $replace = '';
                $JSONDataQuery = json_decode($matches[1][$index]);
                $renderEngine = RenderEngine::createRenderEngine($JSONDataQuery);
                if($renderEngine->queryParameters->queryType == 'error') {
                    continue;
                }
                $replace = $renderEngine->render();
                $new_text = str_replace($singleMatch, $replace, $new_text);
            }
        }
        return new FilterProcessResult($new_text);
    }
}
