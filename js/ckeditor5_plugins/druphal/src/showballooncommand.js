/**
 * @file defines showBalloon, which is executed when you show balloon
 * editor
 */

import { Command } from 'ckeditor5/src/core';
import {
  ContextualBalloon
} from 'ckeditor5/src/ui';

export default class showBalloon extends Command {

    static get requires() {
        return [ContextualBalloon];
    }

    init() {
        this._balloon = this.editor.plugins.get(ContextualBalloon);
    }

    execute(ObjectView) {
      this._showView(ObjectView);
    }

    _getBalloonPositionData() {
      const view = this.editor.editing.view;
      const selection = view.document.selection;
      let target = null;
  
      // Set a target position by converting view selection range to DOM.
      target = () => view.domConverter.viewRangeToDom(
          selection.getFirstRange()
      );
  
      return {
          target
      };
    }
  
    /**
     * 
     * @param {*} ObjectView contextualballoon editor
     */
    _showView(ObjectView) {
      if(this.haltagView !== null && this._balloon.hasView(this.haltagView)) {
        this._balloon.remove(this.haltagView);
      }
  
      const selectedElement = this.editor.model.document.selection.getSelectedElement();
      if(selectedElement !== null && selectedElement.name === 'halTag') {
        var datas = {}
        for(const key of selectedElement.getAttributeKeys()) {
          datas[key] = selectedElement.getAttribute(key);
        }
        this.haltagView = new ObjectView(this.editor.locale, this.editor.config.get('druphal'), datas);
  
        this._balloon.add({
          view: this.haltagView,
          position: this._getBalloonPositionData()
        });
  
        this.listenTo(this.haltagView, 'cancel', eventInfo => {
          this._balloon.remove(this.haltagView);
          this.stopListening(this.haltagView);
          this.haltagView = null;
        });
  
        this.listenTo(this.haltagView, 'save', eventInfo => {
          const { model } = this.editor;
          const newDatas = this.haltagView.datas;
  
          model.change((writer) => {
            for(const key of Object.keys(newDatas)) {
              writer.setAttribute(key, newDatas[key], selectedElement);
            }
            for(const key of selectedElement.getAttributeKeys()) {
              if(!(key in newDatas)) {
                writer.removeAttribute(key, selectedElement);
              }
            }
            this.editor.editing.reconvertItem(selectedElement);
          });
          this._balloon.remove(this.haltagView);
          this.stopListening(this.haltagView);
          this.haltagView = null;
        });

        this.listenTo(this.haltagView, 'showOtherBalloon', eventInfo => {
            this.editor.execute('showBalloon', this.haltagView.otherBalloon);
        })
      }
    }
}