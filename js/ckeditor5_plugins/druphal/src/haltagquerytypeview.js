import {
    createDropdown,
    addListToDropdown,
    Model,
} from 'ckeditor5/src/ui';
import HaltagView from './haltagview';
import { Collection } from 'ckeditor5/src/utils';


/**
 * Contextual balloon editor for query type option.
 */
export default class HaltagQueryTypeView extends HaltagView {
    _buildForm() {
        const { t } = this.locale;
        const MODE_VELUES = {
            author: {
                type_values: {
                    idHal: t('idHal'),
                    classical: t('Name')
                },
                type: 'authorQueryType',
                default: 'classical',
            },
            structure: {
                type_values: {
                    acronym: t('acronym'),
                    classical: t('Full name')
                },
                type: 'structureQueryType',
                default: 'classical',
            },
        }
        const VALUES = MODE_VELUES[this._mode];
        this._buildedForm[VALUES.type] = new createDropdown(this.locale);
        this._buildedForm[VALUES.type].panelView.template.attributes.class.push('druphal-dropdown');
        this._buildedForm[VALUES.type].buttonView.set({
            label: (VALUES.type in this._currentData)?VALUES.type_values[this._currentData[VALUES.type]]:VALUES.type_values[VALUES.default],
            withText: true,
        });
        
        const items = new Collection();
        for(const typeValue in VALUES.type_values) {
            items.add({
                type: 'button',
                model: new Model({
                    id: typeValue,
                    label: VALUES.type_values[typeValue],
                    withText: true,
                })
            })
        }
        this.listenTo(this._buildedForm[VALUES.type], 'execute', eventInfo => {
            const newValue = eventInfo.source.id;
            this._currentData[VALUES.type] = newValue;
            this._buildedForm[VALUES.type].buttonView.label = VALUES.type_values[newValue];
        });
        addListToDropdown(this._buildedForm[VALUES.type], items);
        return Object.values(this._buildedForm);
    }
}
