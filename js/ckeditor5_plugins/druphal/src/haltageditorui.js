/**
 * @file registers the halTag editor toolbar button, balloon contextual editor
 * and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import icon from '../../../../libraries/plugins/druphal/icons/druphal.svg';
import {
  Model,
  createDropdown,
  addListToDropdown,
} from 'ckeditor5/src/ui';
import { Collection } from 'ckeditor5/src/utils';
import HaltagTemplateEngineView from './haltagtemplateengineview';
import HaltagQueryTypeView from './haltagquerytypeview';
import HaltagDoctypesView from './haltagdoctypesview';
import HaltagDatesView from './haltagdatesview';
import HaltagSortView from './haltagsortview';
import HaltagRenderView from './haltagrenderview';
import HaltaginstanceView from './haltaginstanceview';
import HaltaginstancesView from './haltaginstancesview';
import { ClickObserver } from 'ckeditor5/src/engine';

export default class HaltagEditorUI extends Plugin {

  init() {
    const editor = this.editor;
    const editingView = editor.editing.view;
    editingView.addObserver(ClickObserver);
    const viewDocument = editingView.document;
    const { t } = editor.locale;

    // Detect target type on click event to show contextual balloon editor.
    this.listenTo( viewDocument, 'click', (event, data) => {
      const target=data.target;
      for(const className of target.getClassNames()) {
        switch (className) {
          case 'halTagQueryType':
            editor.execute('showBalloon', HaltagQueryTypeView);
            break;
          case 'halTagDoctype':
            editor.execute('showBalloon', HaltagDoctypesView);
            break;
          case 'halTagDates':
            editor.execute('showBalloon', HaltagDatesView);
            break;
          case 'halTagSort':
            editor.execute('showBalloon', HaltagSortView);
            break;
          case 'halTagRender':
            editor.execute('showBalloon', HaltagRenderView);
            break;
          case 'halTagTemplateEngine':
            editor.execute('showBalloon', HaltagTemplateEngineView);
            break;
          case 'halTagInstance':
            editor.execute('showBalloon', HaltaginstanceView);
            break;
          case 'halTagInstances':
            editor.execute('showBalloon', HaltaginstancesView);
            break;
          default:
            if(this.haltagView !== null && this._balloon.hasView(this.haltagView)) {
              this._balloon.remove(this.haltagView);
            }
            break;
        }
      }
    });

    this.haltagView  = null;

    // This will register the halTag editor toolbar buttons.
    editor.ui.componentFactory.add('druphal', (locale) => {
      const dropdownView = createDropdown(locale);

      // Create the toolbar dropdow.
      dropdownView.buttonView.set({
        label: t('DrupHal'),
        icon,
        tooltip: true,
      });

      const items = new Collection();

      // Add add author halTag button.
      items.add({
          type: 'button',
          model: new Model({
              id: 'authorHalButton',
              withText: true,
              label: t('Author'),
          })
      });

      // Add add structure halTag button.
      items.add({
          type: 'button',
          model: new Model({
              id: 'strctureHalButton',
              withText: true,
              label: t('Structure')
          })
      });

      // Add add custom halTag button.
      items.add({
          type: 'button',
          model: new Model({
              id: 'otherHalButton',
              withText: true,
              label: t('Custom')
          })
      });

      addListToDropdown(dropdownView, items);

      // Execute command insertHalTag with correct type.
      this.listenTo(dropdownView, 'execute', eventInfo => {
        switch(eventInfo.source.id) {
          case 'authorHalButton':
            editor.execute('insertHalTag');
            break;
          case 'strctureHalButton':
            editor.execute('insertHalTag', 'structure');
            break;
          case 'otherHalButton':
            editor.execute('insertHalTag', 'query');
            break;
        }
      });

      return dropdownView;
    });
  }
}
