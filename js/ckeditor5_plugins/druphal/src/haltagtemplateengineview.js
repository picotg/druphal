import {
    createDropdown,
    Model,
    addListToDropdown,
} from 'ckeditor5/src/ui';
import HaltagView from './haltagview';
import { Collection } from 'ckeditor5/src/utils';


/**
 * Contextual balloon editor for choosing template engine.
 */
export default class HaltagTemplateEngineView extends HaltagView {
    _buildForm() {
        const templateEngineField = createDropdown(this.locale);
        templateEngineField.panelView.template.attributes.class.push('druphal-dropdown');

        templateEngineField.buttonView.set({
            label: ('template_engine' in this._currentData)?this._currentData.template_engine:'twig',
            withText: true
        })

        const items = new Collection();

        items.add({
            type: 'button',
            model: new Model({
                id: 'twig',
                withText: true,
                label: 'twig',
            })
        })
        items.add({
            type: 'button',
            model: new Model({
                id: 'CSL',
                withText: true,
                label: 'CSL',
            })
        })

        addListToDropdown(templateEngineField, items);

        this.listenTo(templateEngineField, 'execute', eventInfo => {
            var newValue = eventInfo.source.id;
            templateEngineField.buttonView.set({label: newValue});
            if(newValue === 'CSL') {
                this._currentData.template_engine = 'CSL';
                this._currentData.template_filename = 'american-medical-association-10th-edition';
                this._currentData.template_local = 'fr-FR';
                delete this._currentData.template;
                delete this._currentData.result_fields;

            } else {
                delete this._currentData.template_engine
                delete this._currentData.template_filename;
                delete this._currentData.template_local;
                this._currentData.template = '{% for item in items%}%%NEWEDITELINE%%{{ item.citationFull_s|raw }}%%NEWEDITELINE%%%%NEWLINE%%%%NEWLINE%%%%NEWEDITELINE%%{% endfor %}';
                this._currentData.result_fields = 'citationFull_s';
            }
        });

        this._buildedForm['template_engine'] = templateEngineField;
        return Object.values(this._buildedForm);
    }
}