<?php
/**
 * 
 * Render engine using CSL with CiteProc.
 * 
 * @author Gaël PICOT
 * 
 * drupHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 * 
 */
namespace Drupal\druphal\renderEngine;

require_once \Drupal::service('extension.list.module')->getPath('druphal').'/vendor/autoload.php';

use Seboettg\CiteProc\CiteProc;
use Seboettg\CiteProc\StyleSheet;
use stdClass;
use uga\hallib\QueryIterator;

/**
 * Build CSL-JSON from Hal document
 * 
 * @param stdClass $halDoc Hal document
 * @return stdClass CSL-JSON
 */
function CSLJsonFromHalDoc(stdClass $halDoc): stdClass {
    $newElement = new stdClass();
    if(isset($halDoc->authFullName_s)) {
        $i_familly = 0;
        $i_first = 0;
        $newElement->author = [];
        foreach($halDoc->authLastNameFirstName_s as $authLastNameFirstName) {
            $newAuthor = new stdClass();
            $newAuthor->given = "";
            if(isset($halDoc->authLastName_s)&&isset($halDoc->authLastName_s[$i_familly])&&str_starts_with($authLastNameFirstName, $halDoc->authLastName_s[$i_familly])) {
                $newAuthor->family = $halDoc->authLastName_s[$i_familly];
                $i_familly += 1;
            }
            if(isset($halDoc->authFirstName_s)&&isset($halDoc->authFirstName_s[$i_first])&&str_ends_with($authLastNameFirstName, $halDoc->authFirstName_s[$i_first])) {
                $newAuthor->given = $halDoc->authFirstName_s[$i_first];
                $i_first += 1;
            }
            array_push($newElement->author, $newAuthor);
        }
    }
    $newElement->id = $halDoc->halId_s;
    if(isset($halDoc->doiId_s )) $newElement->DOI = $halDoc->doiId_s;
    if(isset($halDoc->docType_s)) {
        switch($halDoc->docType_s) {
            case 'ART':
            case 'ARTREV':
            case 'DATAPAPER':
            case 'BOOKREVIEW':
                $newElement->type = 'article-journal';
            case 'OUV':
            case 'CRIT':
            case 'MANUAL':
            case 'SYNTOUV':
            case 'DICTIONARY':
                $newElement->type = 'book';
                break;
            default:
                $newElement->type = 'article';
                break;
        }
    }
    if(isset($halDoc->title_s)) $newElement->title = $halDoc->title_s[0];
    if(isset($halDoc->uri_s)) $newElement->URL = $halDoc->uri_s;
    if(isset($halDoc->keyword_s)) $newElement->keyword = $halDoc->keyword_s;
    $parameter = 'container-title';
    if(isset($halDoc->journalTitle_s)) $newElement->$parameter = $halDoc->journalTitle_s;
    if(isset($halDoc->journalPublisher_s)) $newElement->publisher = $halDoc->journalPublisher_s;
    $datePart = 'date-parts';
    if(isset($halDoc->publicationDateY_i)) {
        $newElement->issued = new stdClass();
        $newElement->issued->$datePart = [[$halDoc->publicationDateY_i]];
        if(isset($halDoc->publicationDateM_i)) array_push($newElement->issued->$datePart[0], $halDoc->publicationDateM_i);
        if(isset($halDoc->publicationDateD_i)) array_push($newElement->issued->$datePart[0], $halDoc->publicationDateD_i);
    }
    if(isset($halDoc->page_s)) $newElement->page = $halDoc->page_s;
    if(isset($halDoc->volume_s)) $newElement->volume = $halDoc->volume_s;
    if(isset($halDoc->issue_s)) $newElement->issue = $halDoc->issue_s[0];
    return $newElement;
}

/**
 * Build CSL-JSON array from Hal documents in iterable object
 * 
 * @param QueryIterator|array $halData Hal documents in iterable object
 * @return array CSL-JSON array
 */
function CSLJsonFromHalData($halData): array {
    $result = [];
    foreach($halData as $doc) {
        \array_push($result, CSLJsonFromHalDoc($doc->getRowData()));
    }
    return $result;
}

/**
 * CSL render engine
 */
class CSLEngine extends RenderEngine {

    /**
     * localisation
     */
    protected string $template_local = 'fr-FR';

    /**
     * template filename
     */
    protected string $template_filename = '';

    /**
     * constructeur
     * 
     * @param string localisation
     */
    public function __construct(\stdClass $values=null) {
        parent::__construct($values);
        $this->queryParameters->result_fields = 'halId_s,authFirstName_s,authFullName_s,authLastName_s,authMiddleName_s';
        $this->queryParameters->result_fields .= ',docType_s,title_s,doiId_s,uri_s,keyword_s,journalTitle_s,journalPublisher_s';
        $this->queryParameters->result_fields .= ',publicationDateY_i,publicationDateM_i,publicationDateD_i,page_s';
        $this->queryParameters->result_fields .= ',volume_s,issue_s,authLastNameFirstName_s';
    }

    /**
     * {@inheritdoc}
     */
    public function render_datas(array $data = null):string {
        $additionalMarkup = [
            "DOI" => function($DOIItem, $renderedText) {
                if($renderedText !== '') {
                    return '<a href="https://doi.org/' . $DOIItem->DOI . '">' . $renderedText . '</a>';
                }
                else {
                    return '';
                }
            },
            "URL" => function($URLItem, $renderedText) {
                return '<a href="' . $URLItem->URL . '">' . $renderedText . '</a>';
            },
        ];
        if($data === null) {
            $data = new QueryIterator($this->queryParameters->makeQuery());
        }
        if(!file_exists(\Drupal::service('extension.list.module')->getPath('druphal').'/vendor/citation-style-language/styles/'.$this->template_filename.'.csl')) {
            return 'le fichier '.$this->template_filename.'.csl n\'existe pas';
        }
        $CSLJson = CSLJsonFromHalData($data);
        $style = StyleSheet::loadStyleSheet($this->template_filename);
        $citeProc = new CiteProc($style, $this->template_local, $additionalMarkup);
        try {
            return $citeProc->render($CSLJson);
        } catch (\Error $e) {
            return 'Le moteur de rendu CSL n\'arrive pas à utilise la feuille de style.';
        }
    }
}
