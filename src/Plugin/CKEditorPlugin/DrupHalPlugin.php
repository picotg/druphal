<?php

/**
 * 
 * Druphal tag ([hal][/hal]) editor plugin for CkEditor 4.
 * 
 * @author Gaël PICOT
 * 
 * drupHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

namespace Drupal\druphal\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\druphal\DataManager\UpdateHalDataListe;
use Drupal\editor\Entity\Editor;


/**
 * Defines the "DrupHal" plugin.
 *
 * @CKEditorPlugin(
 *   id = "druphal",
 *   label = @Translation("DrupHal affichage de publication Hal"),
 * )
 */
class DrupHalPlugin extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
        $settings = $editor->getSettings();

        $instanceData = json_decode(\Drupal::configFactory()->get('druphal.settings')->get('instance_list'));
        $instanceList = [];
        foreach($instanceData->response->docs as $instance) {
            $instanceList[json_encode([$instance->name, $instance->code])] = $instance->name;
        }
        asort($instanceList);
        $form['default_instance'] = [
            '#type' => 'select',
            '#title' => $this->t('Default instance'),
            '#default_value' => !empty($settings['plugins']['druphal']['default_instance']) ? $settings['plugins']['druphal']['default_instance'] : '',
            '#options' => array_merge(['' => 'aucun'], $instanceList),
        ];
        $form['update'] = [
          '#type' => 'fieldset',
          '#title' => $this->t("Manual data update"),
          '#tree' => TRUE,
        ];
        $form['update']['instance_update'] = [
            '#type' => 'submit',
            '#value' => $this->t('Update instances'),
            '#submit' => [[$this, 'instance_update']],
        ];
        $form['update']['doctype_update'] = [
            '#type' => 'submit',
            '#value' => $this->t('Update doctypes'),
            '#submit' => [[$this, 'doctype_update']],
        ];
        $form['restaure'] = [
          '#type' => 'fieldset',
          '#title' => $this->t("Restaure data"),
          '#tree' => TRUE,
        ];
        $form['restaure']['doctype_restore'] = [
            '#type' => 'submit',
            '#value' => $this->t('Restaure Doctypes'),
            '#submit' => [[$this, 'doctype_restore']],
            '#disabled' => \Drupal::configFactory()->get('druphal.settings')->get('doctype_list_bakup') === '',
        ];
        $form['restaure']['instance_restore'] = [
            '#type' => 'submit',
            '#value' => $this->t('Restaure instances'),
            '#submit' => [[$this, 'instance_restore']],
            '#disabled' => \Drupal::configFactory()->get('druphal.settings')->get('instance_list_bakup') === '',
        ];
        $form['update_status'] = [
            '#type' => 'inline_template',
            '#template' => file_get_contents(\Drupal::service('extension.list.module')->getPath('druphal').'/libraries/templates/maj_status.twig'),
            '#context' => [
                'update_doctype' => \Drupal::configFactory()->get('druphal.settings')->get('update_doctype', time()),
                'update_instance' => \Drupal::configFactory()->get('druphal.settings')->get('update_instance', time()),
                'try_update_doctype' => \Drupal::configFactory()->get('druphal.settings')->get('try_update_doctype', time()),
                'try_update_instance' => \Drupal::configFactory()->get('druphal.settings')->get('try_update_instance', time()),
            ],
        ];

        $form['templates'] = [
          '#type' => 'fieldset',
          '#title' => $this->t("Template management"),
          '#tree' => TRUE,
        ];

        $config = \Drupal::configFactory()->get('druphal.settings');

        $templateList = ['' => 'nouveau'];
        if(!isset($settings['plugins']['druphal']['template_list_string_value'])) {
            $settings['plugins']['druphal']['template_list_string_value'] = json_encode(json_decode($config->get('template_list')));
        }
        $templateListString = $settings['plugins']['druphal']['template_list_string_value'];
        $form['template_list_string_value'] = [
            '#type' => 'hidden',
            '#default_value' => $templateListString,
            '#attributes' => [
                'id' => 'template_list_string_value',
            ],
        ];
        $templateListData = json_decode($templateListString);
        foreach(array_keys(get_object_vars($templateListData)) as $val) {
            $templateList[$val] = $val;
        }
        $form['templates']['template_list'] = [
            '#type' => 'select',
            '#title' => $this->t('Template list'),
            '#options' => $templateList,
            '#default_value' => 'new',
            '#id' => 'drupdhal_template_list',
        ];
        $form['templates']['delete_template'] = [
            '#type' => 'submit',
            '#value' => $this->t('Delete selected template'),
            '#id' => 'delete_template'
        ];
        $form['templates']['template_engine'] = [
            '#type' => 'select',
            '#title' => $this->t('Render engine'),
            '#options' => [
                'CSL' => 'Citation Style Language',
                'twig' => 'twig'
            ],
            '#default_value' => 'CSL',
            '#id' => 'drupdhal_template_engine_list',
        ];
        $form['templates']['template_name'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Name'),
            '#id' => 'drupdhal_template_name',
            '#attributes' => [
                'placeholder' => $this->t('mon template'),
            ],
        ];
        $form['templates']['template_local'] = [
            '#type' => 'textfield',
            '#title' => $this->t('localization'),
            '#default_value' => 'fr-FR',
            '#id' => 'drupdhal_template_local',
            '#attributes' => [
                'placeholder' => $this->t('fr-FR'),
                'class' => ['CSL']
            ],
            '#label_attributes' => [
                'class' => ['CSL']
            ],
        ];
        $form['templates']['template_filename'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Template filename (without .csl)'),
            '#id' => 'drupdhal_template_filename',
            '#attributes' => [
                'placeholder' => $this->t('apa-6th-edition'),
                'class' => ['CSL']
            ],
            '#label_attributes' => [
                'class' => ['CSL']
            ],
        ];
        $form['templates']['template_fields'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Returned field'),
            '#id' => 'drupdhal_template_fields',
            '#attributes' => [
                'placeholder' => $this->t('citationFull_s'),
                'class' => ['twig'],
                'style' => 'display:none;'
            ],
            '#label_attributes' => [
                'class' => ['twig'],
                'style' => 'display:none;'
            ],
        ];
        $form['templates']['template'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Template'),
            '#id' => 'drupdhal_template',
            '#label_attributes' => [
                'class' => ['twig'],
                'style' => 'display:none;'
            ],
            '#attributes' => [
                'placeholder' => $this->t('{% for item in items%}{{ item.citationFull_s|raw}}<br /><br />{% endfor %}}'),
                'class' => ['twig'],
                'style' => 'display:none;'
            ],
        ];
        $form['templates']['reinit_template'] = [
            '#type' => 'submit',
            '#value' => $this->t('Reload default list'),
            '#id' => 'reinit_template',
        ];
        $form['templates']['valid_template_mod'] = [
            '#type' => 'submit',
            '#value' => $this->t('Validate'),
            '#id' => 'valid_template_mod',
        ];
        $form['#attached']['library'][] = 'druphal/template_list';
        $form['#attached']['drupalSettings']['druphal']['template_list'] = $config->get('template_list');

        return $form;
    }

    /**
     * Restaure last instance list.
     */
    public function instance_restore(array &$form, FormStateInterface $form_state) {
        $form_state->disableRedirect();
        $updater = new UpdateHalDataListe('instance');
        $updater->restaure();
    }

    /**
     * Update instance list.
     */
    public function instance_update(array &$form, FormStateInterface $form_state) {
        $form_state->disableRedirect();
        $updater = new UpdateHalDataListe('instance');
        $updater->update();
    }

    /**
     * Update doctypes list.
     */
    public function doctype_update(array &$form, FormStateInterface $form_state) {
        $form_state->disableRedirect();
        $updater = new UpdateHalDataListe('doctype');
        $updater->update();
    }

    /**
     * Restaure last doctypes list.
     */
    public function doctype_restore(array &$form, FormStateInterface $form_state) {
        $form_state->disableRedirect();
        $updater = new UpdateHalDataListe('doctype');
        $updater->restaure();
    }

    /**
     * {@inheritdoc}
     */
    public function getButtons() {
        return [
            'druphal' => [
                'label' => $this->t('druphal'),
                'image' => $this->getModulePath('druphal').'/libraries/plugins/druphal/icons/druphal.ico',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFile() {
        return $this->getModulePath('druphal').'/libraries/plugins/druphal/plugin.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        $settings = $editor->getSettings();
        if(!isset($settings['plugins']['druphal'])) {
            $settings['plugins']['druphal']['default_instance'] = '';
        }
        $doctypeData = json_decode(\Drupal::configFactory()->get('druphal.settings')->get('doctype_list'));
        $doctypes = [];
        foreach($doctypeData->response->result->doc as $doc) {
            array_push($doctypes, [$doc->str[1], $doc->str[0]]);
            if(isset($doc->arr)) {
                foreach($doc->arr->doc as $docL2) {
                    array_push($doctypes, ['->'.$docL2->str[1], $docL2->str[0]]);
                }
            }
        }
        return [
            'default_instance' => $settings['plugins']['druphal']['default_instance'],
            'template_list' => $settings['plugins']['druphal']['template_list_string_value'],
            'doctypes' => json_encode($doctypes)
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getLibraries(Editor $editor) {
        $librairies = ['druphal/druphal'];
        return $librairies;
    }
}
